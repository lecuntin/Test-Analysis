from __future__ import unicode_literals
from django.db import models
from datetime import datetime
from django.contrib.auth.models import AbstractUser
# Create your models here.
# 后台用户信息表
class User(AbstractUser):
    username = models.CharField(u'用户名', max_length=20, null=False, unique=True)
    password = models.CharField(u'密码', max_length=255, null=False)
    UserTrueName = models.CharField(u'用户真实姓名',max_length=20,null=True)
    UserEmail = models.CharField(u'用户邮箱',max_length=100,null=True)
    UserPhone = models.CharField(u'用户电话',max_length=15,null=True)
    enable_flags = models.CharField(u'有效性',max_length=50,null=True)
    role_code = models.CharField(u'用户角色',max_length=50,null=True)
    createDate = models.DateField(u'创建日期',null=True)
    updateDate = models.DateField(u'更新日期',null=True)
    createUser = models.CharField(u'创建用户',max_length=50,null=True)
    updateUser = models.CharField(u'更新用户',max_length=50,null=True)
