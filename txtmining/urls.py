"""txtmining URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from txtmining import views

urlpatterns = [
    url('inputtext', views.inputtext),        # 输入文本
    url('textclusterinput', views.textclusterinput),  # 输入文本

    url('queryrelwords', views.hz_inputtextquerytext),  # 查询汉字相关词

    url('genkeywords', views.genkeywords),        # 取得关键字
    url('textclusterresult', views.textclusterresult),  # 取得关键字

    url(r'genArborByModel', views.genArborByModel),  # 词向量层次聚类分析
    url(r'genArborByKmean', views.genArborByKmean),  # 词向量K均值聚类
    url(r'genArborByCorelation', views.genArborByCorelation),  # 相关网络分析
    url(r'genArborBymatrix', views.genArborBymatrix),  # 矩阵K均值聚类图
    url(r'genArborByCoocurrence', views.genArborByCoocurrence),  # 产生共现矩阵图
    url(r'genArborByKmeanmatrix', views.genArborByKmeanmatrix),  # 产生共现矩阵图
    url(r'editNodes', views.editNodes),  # 产生共现矩阵图

    url(r'RectSelect', views.RectSelect),

    #url('login', views.login),        # 加入关联的函数和应用
    url(r'admin', admin.site.urls),
    url(r'index', views.index),
    url(r'login', views.login),
    url(r'logout', views.logout),
    url(r'register', views.userregist),#进入注册界面
    url(r'userregist', views.userregist),#处理注册信息
    url(r'changepassword', views.chgPWD),  # 修改用户信息界面


    #url(r'^login/', views.login),
    #url(r'^logout/', views.logout),
    url('hanzi/gengraph', views.hz_gengraph),  # 查询汉字相关词
    url('scencedetect/input', views.sencedetect),  # 查询汉字相关词
    url('genKnowledgeNodeList', views.genKnowledgeNodeList),  # 查询汉字相关词

    url('getsenceinfo',views.getsenceinfo), #获取场景检测结果
    url('', views.index),  # 加入关联的函数和应用

]
