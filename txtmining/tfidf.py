# encoding=utf-8
from __future__ import absolute_import
import os
import jieba
jieba.initialize()
import jieba.posseg
from operator import itemgetter

_get_module_path = lambda path: os.path.normpath(os.path.join(os.getcwd(),
                                                 os.path.dirname(__file__), path))

_get_abs_path = lambda path: os.path.normpath(os.path.join(os.getcwd(), path))

DEFAULT_IDF = "txtmining/idf.txt"
DEFAULT_DICT = "txtmining/dict.txt"


class KeywordExtractor(object):

    STOP_WORDS = set((
        "the", "of", "is", "and", "to", "in", "that", "we", "for", "an", "are",
        "by", "be", "as", "on", "with", "can", "if", "from", "which", "you", "it",
        "this", "then", "at", "have", "all", "not", "one", "has", "or", "that"
    ))

    def set_stop_words(self, stop_words_path):
        abs_path = _get_abs_path(stop_words_path)
        if not os.path.isfile(abs_path):
            raise Exception("jieba: file does not exist: " + abs_path)
        content = open(abs_path, 'rb').read().decode('utf-8')
        for line in content.splitlines():
            self.stop_words.add(line)

    def extract_tags(self, *args, **kwargs):
        raise NotImplementedError


class IDFLoader(object):

    def __init__(self, idf_path=None):
        self.path = ""
        self.idf_freq = {}
        self.median_idf = 0.0
        if idf_path:
            self.set_new_path(idf_path)

    def set_new_path(self, new_idf_path):
        if self.path != new_idf_path:
            self.path = new_idf_path
            content = open(new_idf_path, 'rb').read().decode('utf-8')
            self.idf_freq = {}
            for line in content.splitlines():
                word, freq = line.strip().split(' ')
                self.idf_freq[word] = float(freq)
            self.median_idf = sorted(
                self.idf_freq.values())[len(self.idf_freq) // 2]

    def get_idf(self):
        return self.idf_freq, self.median_idf


class TFIDF(KeywordExtractor):

    #def __init__(self, idf_path=None,dict_path=None):
    def __init__(self, idf_path=None, stopwordslst=None, dictwordslst=None):
        jieba.initialize()
        self.tokenizer =   jieba.dt
        #加载自定义字典
        #self.tokenizer.add_word()
        self.tokenizer.load_userdict(DEFAULT_DICT)
        #增加字典词
        if dictwordslst:
            #print(dictwordslst)
            for vv in dictwordslst:
                self.tokenizer.add_word(vv.strip(),300000)
        self.postokenizer = jieba.posseg.dt
        #增加停用词
        self.stop_words = self.STOP_WORDS.copy()
        if stopwordslst:
            #print(stopwordslst)
            for vv in stopwordslst:
                self.stop_words.add(vv.strip())

        #使用自定义IDF
        self.idf_loader = IDFLoader(idf_path or DEFAULT_IDF)
        self.idf_freq, self.median_idf = self.idf_loader.get_idf()

    def set_idf_path(self, idf_path):
        new_abs_path = _get_abs_path(idf_path)
        if not os.path.isfile(new_abs_path):
            raise Exception("jieba: file does not exist: " + new_abs_path)
        self.idf_loader.set_new_path(new_abs_path)
        self.idf_freq, self.median_idf = self.idf_loader.get_idf()

    def extract_tags(self, sentence, topK=None, withWeight=False, allowPOS=(), withFlag=False):
        """
        Extract keywords from sentence using TF-IDF algorithm.
        Parameter:
            - topK: return how many top keywords. `None` for all possible words.
            - withWeight: if True, return a list of (word, weight);
                          if False, return a list of words.
            - allowPOS: the allowed POS list eg. ['ns', 'n', 'vn', 'v','nr'].
                        if the POS of w is not in this list,it will be filtered.
            - withFlag: only work with allowPOS is not empty.
                        if True, return a list of pair(word, weight) like posseg.cut
                        if False, return a list of words
        """
        if allowPOS:
            allowPOS = frozenset(allowPOS)
            words = self.postokenizer.cut(sentence)
        else:
            words = self.tokenizer.cut(sentence)
        freq = {}
        for w in words:
            #print("w=",w)
            if allowPOS:
                if w.flag not in allowPOS:
                    continue
                elif not withFlag:
                    w = w.word
            wc = w.word if allowPOS and withFlag else w
            if len(wc.strip()) < 2 or wc.lower() in self.stop_words:
                continue
            freq[w] = freq.get(w, 0.0) + 1.0
        total = sum(freq.values())
        for k in freq:
            kw = k.word if allowPOS and withFlag else k
            freq[k] *= self.idf_freq.get(kw, self.median_idf) / total

        if withWeight:
            tags = sorted(freq.items(), key=itemgetter(1), reverse=True)
        else:
            tags = sorted(freq, key=freq.__getitem__, reverse=True)
        if topK:
            return tags[:topK]
        else:
            return tags
'''
if __name__ == '__main__':
    tfidf=TFIDF()
    num=1000
    lst=tfidf.extract_tags("深入贯彻党的十九大和十九届二中、三中、四中全会精神，全面落实全国教育大会精神，按照决战决胜脱贫攻坚和收官之年圆满收官的要求，研究部署2020年全国教育信息化工作，交流总结新冠肺炎疫情期间“停课不停学”工作经验，做好教育信息化十年发展规划和“十三五”规划的收官工作，加快推进教育信息化2.0发展。",topK=num, withWeight=False, withFlag=False, allowPOS=('ns', 'n', 'vn', 'v','x'))
    for vv in lst:
        print(vv)
'''
