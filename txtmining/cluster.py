import fasttext
import scipy.cluster.hierarchy as sch
import jieba
import numpy as np

import jieba.posseg as pseg
jieba.enable_parallel(4)




#重要参数：
#materialID = "JYXXH"
#modelfilename = 'txtmining/data/model_XXHZC_skipgram.bin'
#modelfilename=  'data/model_XXHZC_skipgram.bin'
#lastchanged:20210803,chose wiki model
#this is the only model can use now. 2022.4.20
#modelfilename = 'txtmining/data/model_JYXXH_cbow.bin'

#wiki model, sipgram model
modelfilename = 'txtmining/data/model_wiki_skipgram.bin'
 
# 使用CBOW模型
# model = fasttext.load_model('data/model_' + materialID + '_cbow.bin', encoding='utf-8', label_prefix='__label__')  #
#model = fasttext.load_model(modelfilename)  #

from fasttext import load_model
model = load_model(modelfilename)

#将一组向量（词向量或共现向量）进行kmean聚类
def getkmeanresult(wordlist,wordvect,maxcluster):
    from sklearn.cluster import KMeans
    import numpy as np
    X = np.array(wordvect)
    kmeans = KMeans(n_clusters=maxcluster, random_state=0).fit(X)
    wordcluster={}
    for j in range(len(wordlist)):
        wordcluster[wordlist[j]]=kmeans.labels_[j]
    return wordcluster


#将一组向量（词向量或共现向量）进行kmean聚类
def getallkmeanresult(wordlist,wordvect,maxcluster):
    from sklearn.cluster import KMeans
    import numpy as np
    X = np.array(wordvect)
    kmeans = KMeans(n_clusters=maxcluster, random_state=0).fit(X)
    wordcluster = []
    for j in range(len(wordlist)):
        wordcluster.append([wordlist[j], kmeans.labels_[j]])
    return wordcluster

#计算两个关键字词向量的余弦相似度
def getWordRelationNumber(word1,word2):
    vec_a=model[word1]
    vec_b=model[word2]
    """Compute cosine similarity between vec_a and vec_b"""
    result=0
    try:
        result = np.dot( vec_a, vec_b ) / \
           (np.linalg.norm( vec_a ) * np.linalg.norm( vec_b ))
    except:
        print(vec_a,vec_b)
        pass
    return result


HOST = "47.104.71.98"
HZEduDB = "hz_big"
DBAccessUser = "root"
DBAccessPassword = "edq0pl!Vkckki"

#汉字相关词检索：根据查询词找到指定个数相关词
def getWordsbyquery(querytext,wordsnum,showwordsindict):
    print(querytext)
    querytext=querytext.replace("：：", "::").replace("：:", "::").replace(":：", "::")
    ret= []
    if(querytext.find("::")>0):
        querywordlst = querytext.split("::")
        relval=getWordRelationNumber(querywordlst[0],querywordlst[1])
        ret.append([querywordlst[0],querywordlst[1],relval,"&nbsp;"])
        return ret


    lst = model.get_nearest_neighbors(querytext, wordsnum)
    import pymysql
    db=pymysql.connect(host=HOST,user=DBAccessUser,password=DBAccessPassword,database=HZEduDB)
    cursor=db.cursor()
    wordsstr=''
    for vv in lst:
        if wordsstr=="":
            wordsstr="'"+vv[1]+"'"
        else:
            wordsstr=wordsstr+","+"'"+vv[1]+"'"
    print("SELECT 词,解释 FROM hz_big.cihai where 词 in ("+wordsstr+")")
    cursor.execute("SELECT 词,解释 FROM hz_big.cihai where 词 in ("+wordsstr+")")
    rows=cursor.fetchall()
    
    thelst = []
    wordexplain={}
    allowwdlst=[]
    for vv in rows:
        #thelst.append([vv[0],vv[1]])
        allowwdlst.append(vv[0])
        wordexplain[vv[0]]=vv[1]
    '''
    for vv in lst:
        for ll in thelst:
            if(ll[0]==vv[1]):
                ret.append([querytext,vv[1],vv[0],ll[1]])
    '''
    for vv in lst:
        if(showwordsindict==1):
            if(vv[1] in allowwdlst):
                ret.append([querytext,vv[1],vv[0],wordexplain[vv[1]]])
        else:
            if(vv[1] in allowwdlst):
                ret.append([querytext,vv[1],vv[0],wordexplain[vv[1]]])
            else:
                ret.append([querytext,vv[1],vv[0],"&nbsp;"])
    return ret


#生成层次聚类信息
def getNCluster(Z,maxid,N):
    icnt=0
    #首先获得所有节点的父节点
    p= {}
    for ll in Z:
        p[int(ll[0])]=icnt+maxid
        p[int(ll[1])] = icnt + maxid
        icnt=icnt+1

    #
    #得到所有上层节点
    allendlst={}    #所有节点的父节点字典
    index1=0
    theitem=index1
    while theitem<=icnt+maxid:
        #节点的上层节点列表
        endlst=[]
        endlst.append(theitem)
        try:
            #逐层取得子节点的父节点
            while p[theitem]>=maxid:
                theitem=p[theitem]
                endlst.append(theitem)
        except:
            pass
        #将每个节点的编号和父节点列表加入字典
        allendlst[index1]=endlst
        #处理下一个节点
        index1=index1+1
        theitem=index1

    #根据聚类个数取得最上层节点列表
    thenodes=[]
    #从最上层，也就是Z矩阵最小面的元素开始处理，如果不够，再取下一层，直到够了为止
    theindex= len(Z)-1
    while theindex>0:
        #拟加入的节点
        vv=Z[theindex]
        #从已有节点列表中去掉拟加入节点的上层节点
        for mm in thenodes:
            #如果第1个节点在里面了，则去掉
            if(mm in allendlst[int(vv[0])]):
                if mm in thenodes:
                    thenodes.remove(mm)
            #如果第2个节点在里面了，则去掉，注意如果要去掉的节点不存在，则不管
            if(mm in allendlst[int(vv[1])]):
                if mm in thenodes:
                    thenodes.remove(mm)
        #加上拟加入的节点
        thenodes.append(int(vv[0]))
        thenodes.append(int(vv[1]))
        #如果节点数量足够了，则聚类完成
        if len(thenodes)>=N:
            break
        theindex=theindex-1
    #根据聚类结果，取得每个节点类别编号和层次
    theitem = 0
    clusterresult={}
    while theitem < icnt + maxid:
        tmpindex=0
        for vv in thenodes:
            if vv in allendlst[theitem]:
                #为保证中心节点类另为0，其他节点从1开始分类
                #节点层级为其你节点列表元素个数
                levellen = 0
                for kk in allendlst[theitem]:
                    if(kk==vv):
                        break
                    levellen=levellen+1
                #clusterresult[theitem]= [tmpindex+1,int((len(allendlst[theitem])-1)/5)+1]
                clusterresult[theitem] = [tmpindex + 1, int((levellen - 1) / 3) + 1]
                break
            tmpindex=tmpindex+1
        theitem=theitem+1
    #返回节点分类及顶导节点编号
    return clusterresult,p,thenodes

#根据模型，得到给出关键词的聚类
def gencluster(wordlist,wordweight,maxcluster,thetext):
    data=[]
    A = []
    #节点与连线
    nodes = []
    edges = []

    icnt=0
    namedict={}#用于记录节点名称和标签的对应关系

    for vv in wordlist:
        data.append(model[vv])
        A.append('w'+str(icnt))
        namedict[str(icnt)]=vv
        icnt = icnt + 1

    # 进行层次聚类:
    #Z = sch.linkage(data, method='average')
    '''
    Z共有四列组成，第一字段与第二字段分别为聚类簇的编号，在初始距离前每个初始值被从0~n - 1
    进行标识，每生成一个新的聚类簇就在此基础上增加一对新的聚类簇进行标识，第三个字段表示前两个聚类簇之间的距离，第四个字段表示新生成聚类簇所包含的元素的个数。
    https://blog.csdn.net/Andy_shenzl/article/details/83783469?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-2.edu_weight&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-2.edu_weight
    '''
    # 生成点与点之间的距离矩阵,这里用的欧氏距离:
    disMat = sch.distance.pdist(data, 'euclidean')    #euclidean
    #Z = sch.linkage(data, 'ward')
    Z = sch.linkage(disMat, 'ward') #average
    #f = sch.fcluster(Z, t=3, criterion='distance')  # 聚类，这里t阈值的选择很重要
    #f = sch.fcluster(Z, t=2, criterion='inconsistent', depth=2, R=None, monocrit=None)
    #print(f)  # 打印类标签
    maxid=icnt
    maxlevel=10
    thescale=(100/maxlevel)
    #取得分类编号
    #取得每一个节点的聚类信息，以及所有节点的最顶层节点
    clusterresult,p,thenodes=getNCluster(Z, maxid, maxcluster)


    for ll in clusterresult:
        #计算该节点的下层节点，并加入连线
        tmpstr=""
        for mm in p:
            if(str(p[mm])==str(ll)):
                tmpstr=tmpstr+str(mm)+":{length:"+str((10-clusterresult[ll][1])/10)+"},"
        if tmpstr!="":
            tmpstr=str(ll)+":{"+tmpstr+"},"
            edges.append(tmpstr)
        if ll >=maxid:
            if ll in thenodes:#第下类的顶层概念
                nodes.append(str(ll) + ":{'shape':'dot','label':'','group':'0','radius':30,'level':" + str(clusterresult[ll][1]) + ",'show':1,},")
            else:
                nodes.append(str(ll) + ":{'shape':'dot','label':'','group':'" + str(clusterresult[ll][0]) + "','radius':10,'level':" + str(clusterresult[ll][1]) + ",'show':1,},")
        else:
            #如果指定了大小，则用指定大小，否则用最小
            if namedict[str(ll)] in wordweight:
                nodes.append(str(ll) + ":{'shape':'dot','label':'" + namedict[str(ll)] + "','group':'" + str(clusterresult[ll][0]) + "','radius':"+str(wordweight[namedict[str(ll)]])+",'level':" + str(clusterresult[ll][1]) + ",'show':1,},")
            else:
                nodes.append(str(ll) + ":{'shape':'dot','label':'" + namedict[str(ll)] + "','group':'" + str(clusterresult[ll][0]) + "','radius':40,'level':" + str(clusterresult[ll][1]) + ",'show':1,},")
    #加入最中心节点，不再加入，分类显示
    #nodes.append(str(icnt + maxid) + ":{'shape':'dot','label':'','group':'0','radius':60,'level':0,},")

    nodesstr = ""
    for vv in thenodes:
        if (nodesstr == ""):
            nodesstr = str(int(vv)) + ":{}, "
        else:
            nodesstr = nodesstr + str(int(vv)) + ":{}, "
    # 加入最顶层节点的连接，不再加入，分类显示
    #edges.append(str(icnt + maxid) + ":{" + nodesstr + "},")

    #显示结果
    tmpstr=""
    for vv in nodes:
        if tmpstr=="":
            tmpstr=vv
        else:
            tmpstr=tmpstr+"\r\n"+vv
    #print(tmpstr)
    tmpstr2 = ""
    for vv in edges:
        if tmpstr2 == "":
            tmpstr2 = vv
        else:
            tmpstr2 = tmpstr2 + "\r\n" + vv
    #print(tmpstr2)
    #返回节点、边、聚类数量
    ret={"nodes":tmpstr,"edges":tmpstr2,"maxcluster":maxcluster,"thetext":thetext}
    #print(ret)
    return ret

#根据将矩阵的每一第元素聚类
def genClusterFromMatrix(coroccuarr,wordweight,maxcluster,thetext):
    data=[]
    A = []
    #节点与连线
    nodes = []
    edges = []

    icnt=0
    namedict={}
    for vv in coroccuarr:
        #名字对应关系
        namedict[str(icnt)]=vv
        A.append('w' + str(icnt))
        tmpvec=[]
        for ll in coroccuarr:
            tmpvec.append(coroccuarr[vv][ll])
        data.append(tmpvec)
        icnt = icnt + 1

    '''
    for vv in wordlist:
        data.append(model[vv])
        A.append('w'+str(icnt))
        namedict[str(icnt)]=vv
        icnt = icnt + 1
    '''

    # 进行层次聚类:
    #Z = sch.linkage(data, method='average')
    '''
    Z共有四列组成，第一字段与第二字段分别为聚类簇的编号，在初始距离前每个初始值被从0~n - 1
    进行标识，每生成一个新的聚类簇就在此基础上增加一对新的聚类簇进行标识，第三个字段表示前两个聚类簇之间的距离，第四个字段表示新生成聚类簇所包含的元素的个数。
    https://blog.csdn.net/Andy_shenzl/article/details/83783469?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-2.edu_weight&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-2.edu_weight
    '''
    # 生成点与点之间的距离矩阵,这里用的欧氏距离:
    disMat = sch.distance.pdist(data, 'euclidean')    #euclidean
    #Z = sch.linkage(data, 'ward')
    Z = sch.linkage(disMat, 'ward') #average
    #f = sch.fcluster(Z, t=3, criterion='distance')  # 聚类，这里t阈值的选择很重要
    #f = sch.fcluster(Z, t=2, criterion='inconsistent', depth=2, R=None, monocrit=None)
    #print(f)  # 打印类标签
    maxid=icnt
    maxlevel=10
    thescale=(100/maxlevel)
    #取得分类编号
    #取得每一个节点的聚类信息，以及所有节点的最顶层节点
    clusterresult,p,thenodes=getNCluster(Z, maxid, maxcluster)
    for ll in clusterresult:
        #计算该节点的下层节点，并加入连线
        tmpstr=""
        for mm in p:
            if(str(p[mm])==str(ll)):
                tmpstr=tmpstr+str(mm)+":{length:"+str((10-clusterresult[ll][1])/10)+"},"
        if tmpstr!="":
            tmpstr=str(ll)+":{"+tmpstr+"},"
            edges.append(tmpstr)
        if ll >=maxid:
            if ll in thenodes:#第下类的顶层概念
                nodes.append(str(ll) + ":{'shape':'dot','label':'','group':'0','radius':30,'level':" + str(clusterresult[ll][1]) + ",'show':1,},")
            else:
                nodes.append(str(ll) + ":{'shape':'dot','label':'','group':'" + str(clusterresult[ll][0]) + "','radius':20,'level':" + str(clusterresult[ll][1]) + ",'show':1,},")
        else:
            if namedict[str(ll)] in wordweight:
                nodes.append(str(ll) + ":{'shape':'dot','label':'" + namedict[str(ll)] + "','group':'" + str(clusterresult[ll][0]) + "','radius':"+str(wordweight[namedict[str(ll)]])+",'level':" + str(clusterresult[ll][1]) + ",'show':1,},")
            else:
                nodes.append(str(ll) + ":{'shape':'dot','label':'" + namedict[str(ll)] + "','group':'" + str(clusterresult[ll][0]) + "','radius':60,'level':" + str(clusterresult[ll][1]) + ",'show':1,},")
    #加入最中心节点，不再加入，分类显示
    #nodes.append(str(icnt + maxid) + ":{'shape':'dot','label':'','group':'0','radius':60,'level':0,'show':1,},")

    nodesstr = ""
    for vv in thenodes:
        if (nodesstr == ""):
            nodesstr = str(int(vv)) + ":{}, "
        else:
            nodesstr = nodesstr + str(int(vv)) + ":{}, "
    # 加入最顶层节点的连接，不再加入，分类显示
    #edges.append(str(icnt + maxid) + ":{" + nodesstr + "},")

    #显示结果
    tmpstr=""
    for vv in nodes:
        if tmpstr=="":
            tmpstr=vv
        else:
            tmpstr=tmpstr+"\r\n"+vv
    #print(tmpstr)
    tmpstr2 = ""
    for vv in edges:
        if tmpstr2 == "":
            tmpstr2 = vv
        else:
            tmpstr2 = tmpstr2 + "\r\n" + vv
    #print(tmpstr2)
    #返回节点、边、聚类数量
    ret={"nodes":tmpstr,"edges":tmpstr2,"maxcluster":maxcluster,"thetext":thetext}
    #print(ret)
    return ret
'''
if __name__ == '__main__':
    # 分类数量
    maxcluster = 20
    # wordlist=["教育","老师","学生","课程","信息化","资源","平台","教材","城市", "高中", "班", "地方", "农村", "初中", "教师"] #,'计算机','互联网','电脑','国家','智慧教育']
    wordlist = ["教育", "学校", "资源", "建设", "学生", "信息化", "工作", "发展", "管理", "信息", "教师", "加强", "实现", "成都市", "推进", "小学", "国家",
                "支持", "网络", "服务", "应用", "单位", "数据", "开展", "教学", "微课", "建立", "学习", "进行", "中学", "系统", "职业", "完善", "推动",
                "培训", "提高", "实施", "组织", "创新", "教育部", "促进", "信息技术", "论文", "教育局", "技术", "社会", "提供", "制度", "提升", "要求",
                "机制", "四川省", "项目", "能力", "安全", "标准", "坚持", "形成", "方式", "体系", "课程", "相关", "使用", "全面", "机构", "情况", "活动",
                "平台", "加快", "重点", "基本", "实验", "地区", "中国", "企业", "数字", "研究", "保障", "内容", "基础", "模式", "区域", "中小学", "问题",
                "指导", "水平", "中心", "专业", "空间", "规范", "完成", "jy", "评价", "政策", "高校", "方面", "全国", "包括", "人民", "探索", "合作",
                "改革", "制定", "构建", "共享", "规划", "代码", "作用", "部门", "政府", "教育资源", "做好", "人员", "设备", "校园", "统一", "计划", "统筹",
                "参与", "优质", "网络安全", "试点", "具有", "学科", "环境", "利用", "需求", "互联网", "国际", "成都", "信息系统", "地方", "考试", "结合",
                "深化", "实践", "强化", "鼓励", "民族", "培养", "数字化", "确保", "农村", "需要", "家庭", "开发", "文化", "加大", "元素", "规定", "融合",
                "领导", "课堂", "综合", "增强", "支撑", "部署", "业务", "优化", "评估", "名称", "扩大", "质量", "功能", "日期", "任务", "发挥", "领域",
                "科学", "目标", "校区", "师生", "经费", "负责", "经济", "中央", "设计", "提出", "引导", "部分", "工程", "招生", "文件", "设置", "条件",
                "协调", "时间", "战略", "解决", "网站", "学院", "推广", "特色", "报告", "示范", "精神", "开放", "是否", "充分发挥", "人才", "svg", "形式",
                "职业院校", "职责", "gb", "教育厅", "方案", "交流", "维护", "现代化", "通知", "行政部门", "用户", "类型", "办学", "成果", "纳入", "人才培养",
                "过程", "继续", "责任", "电子", "教学点", "力度", "统计", "监测", "信息安全", "分类", "发布", "义务教育", "整合", "高等学校", "意见", "持续",
                "核心", "自主", "协同", "覆盖", "装备", "小学校", "就业", "可选", "确定", "智慧", "基础设施", "采用", "引领", "监管", "分析", "操作",
                "试点工作", "专家", "必选", "运行", "突出", "教研", "社会主义", "四川", "结构", "满足", "办法", "高等教育", "运用", "深入", "课堂教学", "页面",
                "智能", "面向", "社区", "安排", "管理信息系统", "联系人", "现代", "布局", "大学", "科技", "相应", "着力", "幼儿园", "iwb", "宣传", "原则",
                "基础教育", "普及", "属性", "达到", "教学资源", "配备", "附件", "教职工", "依法", "保护", "参加", "建成", "措施", "政治", "资助", "审核",
                "实际", "适应", "行动", "描述", "符合", "教室", "全省", "产品", "班级", "年度", "知识", "关系", "对象", "申请", "行业", "资金", "思想",
                "考生", "范围", "采取", "标签", "整体", "考核", "行为", "时代", "经验", "手段", "培育", "数据项", "教材", "接入", "实行", "课例", "城乡",
                "生活", "校长", "产业", "科研", "投入", "世界", "治理", "举办", "合理", "升级", "个人", "学籍", "共建", "监督", "验收", "宽带", "中小学校",
                "工具", "没有", "毕业生", "决策", "生产", "阶段", "规模", "姓名", "城市", "申报", "重庆市", "软件", "视频", "移动", "文本", "群众", "依托",
                "牵头", "推荐", "能够", "编制", "负责人", "个性化", "行政", "资源共享", "北京市", "湖南省", "专项", "计算机", "采集", "注重", "注册", "农业",
                "调整", "方向", "人数", "科技司", "教学模式", "工业", "数量", "优先", "奖励", "普通高中", "展示", "指标", "设立", "成立", "意识", "方法",
                "理念", "关键", "市场", "主体", "选择", "基地", "大力", "启动", "变革", "公共服务", "简称", "成为", "生态", "电教馆", "会议", "优势",
                "电子白板", "定期", "mjy", "素养", "用于", "增加", "拓展", "国务院", "建立健全", "围绕", ""]
    
    gencluster(wordlist,maxcluster)
'''
