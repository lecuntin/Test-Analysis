# views.py
from django.shortcuts import render,redirect
from . import models
from .forms import UserForm
from txtmining import tfidf
from txtmining import configOperation
#from __future__ import print_function
#取类算法
from txtmining.cluster import *
import base64
from django.shortcuts import render,HttpResponse
import json

from textrank4zh import TextRank4Keyword

#停用词
#stopwords = {}.fromkeys([ line.rstrip() for line in open('txtmining/stopwords.txt') ])

def gettextkeywords(text,num,allowPos,withWeight,stopwordslst,dictwordslst):
    '''
    tr4w = TextRank4Keyword()
    tr4w.analyze(text=text, lower=True, window=2)  # py2中text必须是utf8编码的str或者unicode对象，py3中必须是utf8编码的bytes或者str对象
    wordlist=tr4w.get_keywords(num, word_min_len=2)
    retwordlist=[]
    for item in wordlist:
        retwordlist.append(item.word)
    return retwordlist
    '''
    #import jieba.analyse
    # textrank
    #keywords_textrank = jieba.analyse.textrank(text,num)
    #print("textrank", keywords_textrank)
    #return keywords_textrank
    # tf-idf
    mytfidf = tfidf.TFIDF(stopwordslst=stopwordslst,dictwordslst=dictwordslst)
    keywords_tfidf = mytfidf.extract_tags(text,topK=num, withWeight=withWeight, withFlag=False, allowPOS=(allowPos))  # allowPOS默认为('ns', 'n', 'vn', 'v')
    #print(keywords_tfidf)
    return keywords_tfidf

#登录与退出
from django.contrib.auth import authenticate    #, login, logout
from django.shortcuts import render
from django.shortcuts import redirect
from txtmining.models import *
def userregist(request):
    print(getIP(request))
    config_file_path = "txtmining/system.ini"
    field = "register"
    thefield = "administrator"
    adminname = configOperation.read_config(config_file_path, thefield, "name")
    email = configOperation.read_config(config_file_path, thefield, "email")

    if request.method == 'POST':
        #首先难邀请码
        if (configOperation.read_config(config_file_path, field, "allowregister") == "1"):
            if (configOperation.read_config(config_file_path, field, "needInviteCode") == "1"):
                inviteCode = request.POST['inviteCode']
                if (configOperation.read_config(config_file_path, field, "inviteCode") != inviteCode):
                    message = "邀请码错误，请确保您有正确的邀请码。<br>如果您没有邀请码，请联系管理员：<br>"+adminname+"["+email+"]"+"。"
                    ret = {'retstr': message, "needInviteCode": True}
                    return render(request, 'register.html', ret)

        uf = UserForm(request.POST)
        print("用户注册")
        if uf.is_valid():
            #获得表单数据
            username = uf.cleaned_data['username']
            password = uf.cleaned_data['password']
            print("username,pwd",username,password)
            # createdate=time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
            #添加到数据库
            #判断用户是否已经存在
            count = User.objects.filter(username=username).count()
            if count>0:
                message = "用户名已经存在！"
                ret = {'retstr': message}
                return render(request, 'register.html', ret)

            User.objects.create_user(username= username,password=password)
        return render(request,"login.html")
    else:
        uf = UserForm()

        if (configOperation.read_config(config_file_path, field, "allowregister") == "1"):#开放注册
            if (configOperation.read_config(config_file_path, field, "needInviteCode") == "1"):#需要邀请码


                message = "系统要求输入邀请码，请确保您有正确的邀请码。<br>如果您没有邀请码，请联系管理员：<br>"+adminname+"["+email+"]"+"。"
                ret = {'retstr': message, "needInviteCode": True}
                return render(request, 'register.html', ret)
            else:#不需要邀请码
                message = ""
                ret = {'retstr': message, "needInviteCode": False}
                return render(request, 'register.html', ret)
        else:
            #如果不能注册，则返回提示窗口
            caption="提示"

            message = "系统目前未开放注册。<br>如果您的确需要使用本系统，请联系管理员：<br>"+adminname+"["+email+"]"+"。"
            ret = {'caption': caption,"info":message,'next':"/"}
            return render(request, 'showInfo.html', ret)

def chgPWD(request):
    if request.method == 'POST':
        print("修改密码")
        # 获得表单数据
        #username = request.POST['username']
        username=request.POST['username']   #request.user.username
        password = request.POST['password']
        #print('org uname and pwd', username, password)
        #一定要这样才能改密码。您已创建自定义用户。 将其添加到models.py，views.py，forms.py中
        from django.contrib.auth import get_user_model
        User = get_user_model()
        u = User.objects.get(username=username)
        u.set_password(password)
        u.save()
        message = "密码修改成功！"
        from django.http import HttpResponseRedirect
        return HttpResponseRedirect("/index")
    else:
        uf = UserForm()
    return render(request,"changepassword.html")


# 返回templates中的login.html文件
def login(request):
    from urllib import parse
    #先得到原来的URL，并取得其参数；再得到其next值
    url=request.META.get('HTTP_REFERER', '/')
    params = parse.parse_qs(parse.urlparse(url).query)
    try:
        next = params['next'][0]
    except:
        next="/index"
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user_obj = authenticate(username=username, password=password)
        #if user_obj:
        if user_obj is not None and user_obj.is_active:
            from django.contrib import auth
            from django.http import HttpResponseRedirect
            auth.login(request, user_obj)
            #登录成功后，返回
            return HttpResponseRedirect(next)
        else:
            message="登录失败"
            username=""
            ret = {'retstr': message,"username":username}
            return render(request, 'login.html', ret)
    else:
        return render(request, 'login.html')

def logout(request):
    print("退出")
    from urllib import parse
    # 先得到原来的URL，并取得其参数；再得到其next值
    url = request.META.get('HTTP_REFERER', '/')
    params = parse.parse_qs(parse.urlparse(url).query)
    try:
        next = params['next'][0]
    except:
        next = "/index"

    from django.contrib import auth
    auth.logout(request)
    from django.http import HttpResponseRedirect
    if next == "":
        return HttpResponseRedirect("/index")
    else:
        return HttpResponseRedirect(next)

def index(request):
    print("首页", getIP(request))
    return  render(request, 'index.html')

#1.输入待处理的文本
from django.contrib.auth.decorators import login_required

#文本聚糖类入口
@login_required
def textclusterinput(request):
    print("文本聚类分析", getIP(request))
    return  render(request, 'textcluster.html')

#文本分析入口
@login_required
def inputtext(request):
    print("文本分析", getIP(request))
    return  render(request, 'inputtext.html')

#节点编码入口
@login_required
def editNodes(request):
    print("节点编辑", getIP(request))
    ret = {"nodes": "", "edges": "", "maxcluster": 0}
    return render(request, 'showclusterresult.html', {'ret': ret})

#场景识别入口
@login_required
def sencedetect(request):
    print("场景识别", getIP(request))
    return render(request, '../templates/scencedetect/sencedetect.html')

#auto generate knowledge node.
@login_required
def genKnowledgeNodeList(request):
    return render(request, '../templates/genKnowledgeNodeList.html')

#获取request IP地址
def getIP(request):
    if request.META.get('HTTP_X_FORWARDED_FOR'):
        ip = request.META.get("HTTP_X_FORWARDED_FOR")
    else:
        ip = request.META.get("REMOTE_ADDR")
    return ip
@login_required
def hz_inputtextquerytext(request):
    print("汉字词语查询",getIP(request))
    if request.method == "GET":
        #如果没有传数据，返回原界面
        return render(request, 'hanzi/inputtext.html')
    elif request.method == "POST":
        querytext = request.POST.get('querytext')
        #print(querytext)
        relwordsnum = request.POST.get('relwordsnum')
        showwordsindict = request.POST.get('showwordsindict')
        if(showwordsindict==None):
            showwordsindict=0
        else:
            showwordsindict=1
        #拆分输入串
        #querytext2 = querytext.replace("，", ",").replace(" ", ",").replace("　", ",").replace("；", ",").replace(";", ",").replace("\r\n", ",")
        #querytext = querytext2
        #querywordlst = querytext.split(",")
        querywordlst = querytext.split("\r\n")
        allwordarr = []
        mytfidf = tfidf.TFIDF()
        for ll in querywordlst:
            if ll.strip()!="":
                #print("ll=",ll.strip(),",","relwordsnum=",relwordsnum)
                thelst = getWordsbyquery(ll.strip(), int(relwordsnum),showwordsindict)
                onwordarr=[]
                for mm in thelst:
                    theidf = mytfidf.idf_freq.get(mm[1], mytfidf.median_idf)
                    #theval = round(mm[2]*100 / theidf, 2)       # 模仿TF-IDF算法，排序为相关系数/IDF，即：mm[2] / theidf
                    theval = round(mm[2]*100, 1)       # 模仿TF-IDF算法，排序为相关系数/IDF，即：mm[2] / theidf
                    onwordarr.append({"word1": mm[0], "word2": mm[1], "strength": theval,"explain":mm[3]})
                # 数组高级排序 https://www.cnblogs.com/monsteryang/p/6938779.html
                #print(onwordarr)
                onwordarr.sort(key=lambda x: x['strength'], reverse = True)
                #print(onwordarr)
                for mm in onwordarr:
                    allwordarr.append({"word1": mm["word1"], "word2": mm["word2"], "strength": mm["strength"],"explain": mm["explain"]})
        retstr = "<table border=1 cellpadding=12 cellspacing=0 style='width:100%;'><tr>" #font-size:20px;
        cntperrow = 1
        i = 0

        while i < cntperrow:
            retstr = retstr + "<td align=center width=50px style='padding:2px;font-weight:bold;'>编号</td><td align=center  width=70px style='padding:2px;font-weight:bold;'>原词</td>" \
                              "<td align=center width=70px style='padding:2px;font-weight:bold;'>相关词</td><td align=center  width=50px style='padding:2px;font-weight:bold;'>相关系数</td><td align=center  width=580px style='padding:2px;font-weight:bold;'>解释</td>"
            i += 1
        retstr = retstr + "</tr>"
        icnt = 0
        for vv in allwordarr:
            contentstr = "<td align=center style='padding:2px;'>" + str(icnt + 1) + "</td><td align=right style='padding-right:10px;'>" + vv["word1"] + "</td><td align=left style='padding-left:10px;'>" + vv["word2"] + "</td><td align=center style='padding:2px;'>" + str(vv["strength"]) + "</td><td align=left style='padding:2px;'>" + str(vv["explain"]) + "</td>"
            if ((icnt % cntperrow) == 0):
                if (icnt == 0):
                    retstr = retstr + "</tr><tr>" + contentstr
                else:
                    retstr = retstr + "<tr>" + contentstr
            else:
                retstr = retstr + contentstr
            icnt = icnt + 1
        while 1:
            if ((icnt % cntperrow) == 0):
                break
            else:
                retstr = retstr + "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>"
            icnt = icnt + 1
        retstr = retstr + "</tr></table>"
        ret={'retstr':retstr}
        return render(request, 'hanzi/chosekeywords.html',ret)
    else:
        # PUT,DELETE,HEAD,OPTION...
        return render(request, 'hanzi/inputtext.html')

def RectSelect(request):
    return render(request, 'RectSelect.html')
    pass

#得到场景识别结果
def getsenceinfo(request):
    if request.method == 'POST':
        # encoding:utf-8
        import requests
        import base64
        APIKey = "H57CxYDc4VDyor6XNRlfH7P5"
        SecretKey = "MrvPiOoijpPv5txggb0jDh8maFcOqnor"
        retinfo="<tr><td align=center style='border-left:1px solid white;border-top:1px solid white;'>分类</td>" \
                "<td align=center style='border-top:1px solid white;'>场景描述</td>" \
                "<td align=center style='border-top:1px solid white;'>可能性</td></tr>"
        # client_id 为官网获取的AK， client_secret 为官网获取的SK
        host = 'https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=' + APIKey + '&client_secret=' + SecretKey
        response = requests.get(host)
        access_token=""
        if response:
            ret = response.json()
            access_token = ret["access_token"]

        request_url = "https://aip.baidubce.com/rest/2.0/image-classify/v2/advanced_general"
        # 二进制方式打开图片文件
        img = bytes(request.POST.get("Filestr")[22:], encoding="utf-8")
        # 文件扩展名
        ext = ".png"
        # 文件目录
        d = "media/scencedetect/"
        # 定义文件名，年月日时分秒随机数
        import os, time, random
        fn = time.strftime('%Y%m%d%H%M%S')
        fn = fn + '_%d' % random.randint(0, 1000)
        # 重写合成文件名
        name = os.path.join(d, fn + ext)
        f = open(name, "wb")
        f.write(base64.b64decode(img))
        f.close()
        #记录分类信息
        #文件名／分类／关键词／可能性

        #print(request.POST.get("Filestr"))
        #print(img)
        params = {"image": img}
        request_url = request_url + "?access_token=" + access_token
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        response = requests.post(request_url, data=params, headers=headers)
        retlst=[]
        tmplst={}
        if response:
            #print(response.json())
            ret = response.json()["result"]
            #画图
            maxitem=0
            maxlen=300
            for vv in ret:
                if vv["score"]>maxitem:
                    maxitem=vv["score"]

            for vv in ret:
                retinfo =retinfo+"<tr><td style='padding:2px;border-left:1px solid white;'>"+vv["root"]+"</td>"
                retinfo =retinfo+"<td style='padding:2px;'>"+vv["keyword"]+"</td>"
                retinfo =retinfo+"<td style='padding:2px;' valign='middle'>"
                retinfo =retinfo+"<div style='background-color:#9F9F9F;font-size:11.5px;border:0px solid red;height:13px;width:"+str(maxlen*vv["score"]/maxitem)+"'>"+str(vv["score"]*100)[0:4]+"</div></td></tr>"

        retinfo="<center><table border=1 cellpadding=5px cellspacing=0 width=100% style='padding:0px;'>"+retinfo+"</table></center>"
        return HttpResponse(json.dumps(retinfo))

#得到文本聚类结果
def textclusterresult(request):
    print("文本聚类",getIP(request))
    if request.method == "GET":
        #如果没有传数据，返回原界面
        return render(request, 'textcluster.html')
    elif request.method == "POST":
        thetext=request.POST.get('thetext')
        stopwords=request.POST.get('stopwords')
        stopwordslst=stopwords.split("\r\n")
        dictwords = request.POST.get('dictwords')
        dictwordslst = dictwords.split("\r\n")
        import jieba
        jieba.initialize()
        #add dict words
        for vv in dictwordslst:
            jieba.add_word(vv.strip(),300000)        
        chkonlycate = request.POST.get('chkonlycate')
        chkmixed = request.POST.get('chkmixed')

        clusternum=request.POST.get('keywordsnum')
        wordlist = thetext.split("\r\n")
        wordvect = []
        for vv in wordlist:
            thestr1=""
            seg_list = jieba.cut(vv,cut_all=False)  
            wdvect=[len(model["好"])]
            #print(wdvect)
            tmpicnt=0
            for sgl in seg_list:
                if sgl not in stopwordslst:
                    tmpicnt=tmpicnt+1
                    wdvect=wdvect+model[sgl]
                    if thestr1=="":
                        thestr1=sgl
                    else:
                        thestr1=thestr1+sgl
            #print(wdvect/tmpicnt)
            #print(model[thestr1])
            '''
            import jieba
            wordlst=jieba.cut(vv)
            iwordcnt=0
            wdvect=[len(model["好"])]
            for wd in wordlst:
                iwordcnt=iwordcnt+1
                wdvect=wdvect+model[wd]
            wdvect=wdvect/iwordcnt
            wordvect.append(wdvect)
            '''
            #wordvect.append(model[vv])
            wordvect.append(model[thestr1])
        wordcluster = getallkmeanresult(wordlist, wordvect, int(clusternum))
        #记录分类中材料个数、分类中的文本，以便提取关键词
        catecount= []
        catestrarr=[]
        for i in range(int(clusternum)):
            #显示分类
            catecount.append(0)
            catecount[i]=0
            tmpstr = ""
            for vv in wordcluster:
                if vv[1] == i:
                    catecount[i]=catecount[i]+1
                    tmpstr=tmpstr+"\r\n  "+vv[0]
            catestrarr.append(tmpstr)
            #catestrarr[i]=tmpstr

        tmpstr=""
        for i in range(int(clusternum)):
            #显示分类
            thekeywords=gettextkeywords(text=catestrarr[i],num=10,withWeight=False, allowPos=None,stopwordslst=stopwordslst,dictwordslst=dictwordslst)
            keywordsstr=""
            for vv in thekeywords:
                keywordsstr =keywordsstr+" "+vv
            if str(chkonlycate) == "1":
                tmpstr=tmpstr+"\r\n分类：" + str(i+1)+"，共有["+str(catecount[i])+"]项。关键词：" +keywordsstr+ "\r\n"+ "-" * 110
            else:
                if str(chkmixed) == "1":
                    for vv in wordcluster:
                        if vv[1] == i:
                            #catecount[i]=catecount[i]+1
                            tmpstr=tmpstr+"\r\n分类：" + str(i+1)+"，共有["+str(catecount[i])+"]项。关键词：" +keywordsstr.strip()+ "|"+vv[0]
                else:
                    tmpstr = tmpstr + "\r\n" + "-" * 110 + "\r\n分类：" + str(i+1)+"，共有["+str(catecount[i])+"]项。关键词：" +keywordsstr+ "\r\n" + "-" * 110
                    for vv in wordcluster:
                        if vv[1] == i:
                            #catecount[i]=catecount[i]+1
                            tmpstr=tmpstr+"\r\n  "+vv[0]

        ret = {'retstr': tmpstr}
        return render(request, 'textclusterresult.html', ret)
    else:
        # PUT,DELETE,HEAD,OPTION...
        return render(request, 'textcluster.html')
    return


#提交文本，得到其关键字列表，转入选择关键词界面
def genkeywords(request):
    #处理参考：https://blog.51cto.com/beanxyz/1944978?cid=719693
    print("文本聚类",getIP(request))
    #getLACresult()
    if request.method == "GET":
        #如果没有传数据，返回原界面
        return render(request, 'inputtext.html')
    elif request.method == "POST":
        #待分析文本
        thetext=request.POST.get('thetext')
        stopwords=request.POST.get('stopwords')
        stopwordslst=stopwords.split("\r\n")
        dictwords = request.POST.get('dictwords')
        dictwordslst = dictwords.split("\r\n")
        synwords = request.POST.get('synwords')
        if synwords:
            synwordslst = synwords.split("\r\n")
            for dd in synwordslst:
                if (dd==""):
                    continue
                dd=dd.strip()
                dd=dd.replace("；"," ")
                dd=dd.replace(";"," ")
                dd=dd.replace("，"," ")
                dd=dd.replace("　"," ")
                dd=dd.replace("，"," ")
                dd=dd.replace(","," ")
                dd=dd.replace("、"," ")
                dd=dd.replace("。"," ")
                dd=dd.replace("."," ")
                dd=dd.replace("  "," ")
                dd=dd.replace("  "," ")
                ddlist=dd.split(" ")
                thetext=thetext.replace(ddlist[1],ddlist[0])
        keywordsnum=request.POST.get('keywordsnum')
        #候选词性列表
        poss = request.POST.getlist('addpos')
        allPos = set()
        for vv in poss:
            allPos.add(vv)
        #限制词性
        thelst=gettextkeywords(text=thetext,num=int(keywordsnum), withWeight=True, allowPos=allPos,stopwordslst=stopwordslst,dictwordslst=dictwordslst)
        retstr = "" #
        retstr2 = "<center><table cellpadding=2 cellspacing=0 width=700px border=0 ><tr style='border-bottom:1px solid gray;'><td align=center>序号</td><td align=center>关键词</td><td align=center>重要性</td></tr>" #font-size:20px;

        cntperrow=6
        icnt=0
        #print(thelst)
        for vv in thelst:
            icnt=icnt+1  
            retstr2=retstr2+"<tr style='border-bottom:1px solid gray;'><td width=50px>"+str(icnt)+"</td><td width=150px  align=right>"+str(vv[0])+"&nbsp;</td><td width=500px><div style='background-color:#9F9F9F;font-size:11.5px;border:0px solid red;height:13px;width:"+str(500*(vv[1]*100)/100)+"'>"+str(int(vv[1]*10000)/100)+"</div></td></tr>"
            if retstr=="":
                retstr=str(vv[0])
            else:  
                retstr=retstr+" "+str(vv[0])
        retstr2=retstr2+"</table></center>"
        ret={'retstr':retstr,'thetext':thetext,'stopwords':stopwords,'synwords':synwords,'dictwords':dictwords,'thelst':retstr2}
        #进入下一步，生成待选择的关键词（按重要性从高到低排列好）
        return render(request, 'chosekeywords.html',ret)
    else:
        # PUT,DELETE,HEAD,OPTION...
        return render(request, 'inputtext.html')

def genArborByKmean(request):
    print("生成词向量K均值聚类图形",getIP(request))
    if request.method == "GET":
        #如果没有传数据，返回原界面
        return render(request, 'inputtext.html')
    elif request.method == "POST":
        wordliststr = request.POST.get('keywordlist')
        wordliststr=wordliststr.strip()
        wordliststr=wordliststr.replace("；"," ")
        wordliststr=wordliststr.replace(";"," ")
        wordliststr=wordliststr.replace("，"," ")
        wordliststr=wordliststr.replace("　"," ")
        wordliststr=wordliststr.replace("，"," ")
        wordliststr=wordliststr.replace(","," ")
        wordliststr=wordliststr.replace("、"," ")
        wordliststr=wordliststr.replace("。"," ")
        wordliststr=wordliststr.replace("."," ")
        wordliststr=wordliststr.replace("  "," ")
        wordliststr=wordliststr.replace("  "," ")
        wordliststr=wordliststr.replace("  "," ")
        wordlist=wordliststr.split(" ")

        wordvect = []
        for vv in wordlist:
            wordvect.append(model[vv])
        wordcluster = {}
        clusternum = request.POST.get('clusternum')
        wordcluster=getkmeanresult(wordlist,wordvect,int(clusternum))
        sortedwordcluster = sorted(wordcluster.items(), key=lambda item: item[1])

        #气泡图大小规一化
        thetext = request.POST.get('thetext')
        thetext1 = request.POST.get('thetext')
        thetext=thetext.replace("\r\n","")
        stopwords = request.POST.get('stopwords')
        if stopwords:
            stopwords = stopwords.strip()
            stopwordslst = stopwords.split("\r\n")
        else:
            stopwordslst = None
        dictwords = request.POST.get('dictwords')
        if dictwords:
            dictwords = dictwords.strip()
            dictwordslst = dictwords.split("\r\n")
        else:
            dictwordslst = None

        wordweight={}
        maxwordweight=0
        minwordweight=0
        '''
        mytfidf = tfidf.TFIDF()
        keywords_tfidf = mytfidf.extract_tags(thetext, topK=None,withWeight=True, withFlag=False)  # allowPOS=('n','x','nr','ns','nsf','nt','nl','ng','s','v')  allowPOS默认为('ns', 'n', 'vn', 'v')
        '''
        keywords_tfidf=gettextkeywords(text=thetext, num=None, withWeight=True,allowPos=None,stopwordslst=stopwordslst,dictwordslst=dictwordslst)
        for vv in keywords_tfidf:
            if vv[0] in wordlist:
                wordweight[vv[0]]=vv[1]
                if vv[1]>maxwordweight:
                    maxwordweight=vv[1]
                if vv[1]<minwordweight:
                    minwordweight=vv[1]
        #规一化为40-120
        minnormal=40
        maxnormal=100
        for vv in wordweight:
            wordweight[vv]=minnormal+(wordweight[vv]*(maxnormal-minnormal))/(maxwordweight-minwordweight)
        icnt = 0
        nowgrpid = 0
        nodes = []
        edges = []
        irow = 0
        tmpstr = ""
        #print(sortedwordcluster)
        for vv in sortedwordcluster:
            # 首先生成节点
            if vv[0] in wordweight:
                nodes.append(str(irow) + ":{'shape':'dot','label':'" + vv[0] + "','group':'" + str(vv[1]) + "','radius':"+str(wordweight[vv[0]])+",'level':1,'show':1,},")
            else:
                nodes.append(str(irow) + ":{'shape':'dot','label':'" + vv[0] + "','group':'" + str(vv[1]) + "','radius':40,'level':1,'show':1,},")
            if nowgrpid != vv[1]:  # 换组了，生成组节点连线
                nowgrpid = vv[1]
                if tmpstr != "":
                    #生成中心节点
                    nodes.append("grp" + str(irow) + ":{'shape':'dot','label':'','group':'0','radius':10,'level':1,'show':1,},")
                    tmpstr = "grp" + str(irow) + ":{" + tmpstr + "},"
                    edges.append(tmpstr)
                    tmpstr = ""
                    tmpstr = tmpstr + str(irow) + ":{weight:1,},"
            else:
                tmpstr = tmpstr + str(irow) + ":{weight:1,},"
            irow = irow + 1
            #print(thelist)
        #最后一组连线
        if tmpstr != "":
            # 生成中心节点
            nodes.append(
                "grp" + str(irow) + ":{'shape':'dot','label':'','group':'0','radius':10,'level':1,'show':1,},")
            tmpstr = "grp" + str(irow) + ":{" + tmpstr + "},"
            edges.append(tmpstr)
            tmpstr = ""

        # 返回节点、边、聚类数量
        for vv in nodes:
            if tmpstr == "":
                tmpstr = vv
            else:
                tmpstr = tmpstr + "\r\n" + vv
        # print(tmpstr)
        tmpstr2 = ""
        for vv in edges:
            if tmpstr2 == "":
                tmpstr2 = vv
            else:
                tmpstr2 = tmpstr2 + "\r\n" + vv
        # print(tmpstr2)
        # 返回节点、边、聚类数量
        ret = {"nodes": tmpstr, "edges": tmpstr2, "maxcluster": clusternum, "thetext":thetext1}
        return render(request, 'showclusterresult.html', {'ret': ret})
    else:
        # PUT,DELETE,HEAD,OPTION...
        return render(request, 'inputtext.html')

#根据模型取向量生成图形
def genArborByModel(request):
    print("生成图形",getIP(request))
    if request.method == "GET":
        #如果没有传数据，返回原界面
        return render(request, 'inputtext.html')
    elif request.method == "POST":
        wordliststr = request.POST.get('keywordlist')
        wordliststr=wordliststr.strip()
        wordliststr=wordliststr.replace("；"," ")
        wordliststr=wordliststr.replace(";"," ")
        wordliststr=wordliststr.replace("，"," ")
        wordliststr=wordliststr.replace("　"," ")
        wordliststr=wordliststr.replace("，"," ")
        wordliststr=wordliststr.replace(","," ")
        wordliststr=wordliststr.replace("、"," ")
        wordliststr=wordliststr.replace("。"," ")
        wordliststr=wordliststr.replace("."," ")
        wordliststr=wordliststr.replace("  "," ")
        wordliststr=wordliststr.replace("  "," ")
        wordliststr=wordliststr.replace("  "," ")
        wordlist=wordliststr.split(" ")
        thetext = request.POST.get('thetext')
        thetext1 = request.POST.get('thetext')
        thetext=thetext.replace("\r\n","")
        stopwords = request.POST.get('stopwords')
        if stopwords:
            stopwords=stopwords.strip()
            stopwordslst = stopwords.split("\r\n")
        else:
            stopwordslst=None
        dictwords = request.POST.get('dictwords')
        if dictwords:
            dictwords=dictwords.strip()
            dictwordslst = dictwords.split("\r\n")
        else:
            dictwordslst=None

        wordweight={}
        maxwordweight=0
        minwordweight=0
        '''
        mytfidf = tfidf.TFIDF()
        keywords_tfidf = mytfidf.extract_tags(thetext, topK=None,withWeight=True, withFlag=False)  # allowPOS=('n','x','nr','ns','nsf','nt','nl','ng','s','v')  allowPOS默认为('ns', 'n', 'vn', 'v')
        '''
        keywords_tfidf = gettextkeywords(text=thetext, num=None, withWeight=True, allowPos=None,stopwordslst=stopwordslst,dictwordslst=dictwordslst)
        for vv in keywords_tfidf:
            if vv[0] in wordlist:
                wordweight[vv[0]]=vv[1]
                if vv[1]>maxwordweight:
                    maxwordweight=vv[1]
                if vv[1]<minwordweight:
                    minwordweight=vv[1]
        #规一化为40-120
        minnormal=60
        maxnormal=150
        for vv in wordweight:
            wordweight[vv]=minnormal+(wordweight[vv]*(maxnormal-minnormal))/(maxwordweight-minwordweight)
        clusternum=request.POST.get('clusternum')
        ret=gencluster(wordlist,wordweight,int(clusternum),thetext1)
        #print(ret["nodes"])
        #进入下一步，生成待选择的关键词（按重要性从高到低排列好）
        return render(request, 'showclusterresult.html',{'ret':ret})
    else:
        # PUT,DELETE,HEAD,OPTION...
        return render(request, 'inputtext.html')

#共现K均值聚类分析，生成
def genArborByKmeanmatrix(request):
    print("生成共现K均值聚类分析结果",getIP(request))
    if request.method == "GET":
        #如果没有传数据，返回原界面
        return render(request, 'inputtext.html')
    elif request.method == "POST":
        thetext=request.POST.get('thetext')
        clusternum = request.POST.get('clusternum')
        wordliststr = request.POST.get('keywordlist')
        wordliststr=wordliststr.strip()
        wordliststr=wordliststr.replace("；"," ")
        wordliststr=wordliststr.replace(";"," ")
        wordliststr=wordliststr.replace("，"," ")
        wordliststr=wordliststr.replace("　"," ")
        wordliststr=wordliststr.replace("，"," ")
        wordliststr=wordliststr.replace(","," ")
        wordliststr=wordliststr.replace("、"," ")
        wordliststr=wordliststr.replace("。"," ")
        wordliststr=wordliststr.replace("."," ")
        wordliststr=wordliststr.replace("  "," ")
        wordliststr=wordliststr.replace("  "," ")
        wordliststr=wordliststr.replace("  "," ")
        wordlist=wordliststr.split(" ")
        # 准备语料
        doclist = thetext.split("\r\n")
        # 首先生成字典
        coroccuarr = {}
        for vv in wordlist:
            onwordarr = {}
            for mm in wordlist:
                onwordarr[mm] = 0
            coroccuarr[vv] = onwordarr
        #print(coroccuarr)

        for vv in doclist:
            # 每行分词，计算共现次数
            vv = vv.strip()
            if (vv == ""):
                continue
            words = pseg.cut(vv)
            for j in words:
                if j.word in wordlist:
                    for k in words:
                        if k.word in wordlist:
                            coroccuarr[j.word][k.word] = coroccuarr[j.word][k.word] + 1

        wordvect = []
        for vv in coroccuarr:
            wordvect.append(coroccuarr[vv])

        wordcluster = {}
        clusternum = request.POST.get('clusternum')
        wordcluster = getkmeanresult(wordlist, wordvect, int(clusternum))
        sortedwordcluster = sorted(wordcluster.items(), key=lambda item: item[1])

        # 气泡图大小规一化
        thetext = request.POST.get('thetext')
        thetext1 = request.POST.get('thetext')
        thetext = thetext.replace("\r\n", "")
        stopwords = request.POST.get('stopwords')
        if stopwords:
            stopwords = stopwords.strip()
            stopwordslst = stopwords.split("\r\n")
        else:
            stopwordslst = None
        dictwords = request.POST.get('dictwords')
        if dictwords:
            dictwords = dictwords.strip()
            dictwordslst = dictwords.split("\r\n")
        else:
            dictwordslst = None
        wordweight = {}
        maxwordweight = 0
        minwordweight = 0
        '''
        mytfidf = tfidf.TFIDF()
        keywords_tfidf = mytfidf.extract_tags(thetext, topK=None, withWeight=True,
                                              withFlag=False)  # allowPOS=('n','x','nr','ns','nsf','nt','nl','ng','s','v')  allowPOS默认为('ns', 'n', 'vn', 'v')
        '''
        keywords_tfidf = gettextkeywords(text=thetext, num=None, withWeight=True, allowPos=None,stopwordslst=stopwordslst,dictwordslst=dictwordslst)
        for vv in keywords_tfidf:
            if vv[0] in wordlist:
                wordweight[vv[0]] = vv[1]
                if vv[1] > maxwordweight:
                    maxwordweight = vv[1]
                if vv[1] < minwordweight:
                    minwordweight = vv[1]
        # 规一化为40-120
        minnormal = 40
        maxnormal = 100
        for vv in wordweight:
            wordweight[vv] = minnormal + (wordweight[vv] * (maxnormal - minnormal)) / (maxwordweight - minwordweight)
        icnt = 0
        nowgrpid = 0
        nodes = []
        edges = []
        irow = 0
        tmpstr = ""
        #print(sortedwordcluster)
        for vv in sortedwordcluster:
            # 首先生成节点
            if vv[0] in wordweight:
                nodes.append(
                    str(irow) + ":{'shape':'dot','label':'" + vv[0] + "','group':'" + str(vv[1]) + "','radius':" + str(
                        wordweight[vv[0]]) + ",'level':1,'show':1,},")
            else:
                nodes.append(str(irow) + ":{'shape':'dot','label':'" + vv[0] + "','group':'" + str(
                    vv[1]) + "','radius':40,'level':1,},")
            if nowgrpid != vv[1]:  # 换组了，生成组节点连线
                nowgrpid = vv[1]
                if tmpstr != "":
                    # 生成中心节点
                    nodes.append("grp" + str(irow) + ":{'shape':'dot','label':'','group':'0','radius':10,'level':1,'show':1,},")
                    tmpstr = "grp" + str(irow) + ":{" + tmpstr + "},"
                    edges.append(tmpstr)
                    tmpstr = ""
                    tmpstr = tmpstr + str(irow) + ":{weight:1,},"
            else:
                tmpstr = tmpstr + str(irow) + ":{weight:1,},"
            irow = irow + 1
            # print(thelist)
        # 最后一组连线
        if tmpstr != "":
            # 生成中心节点
            nodes.append(
                "grp" + str(irow) + ":{'shape':'dot','label':'','group':'0','radius':10,'level':1,'show':1,},")
            tmpstr = "grp" + str(irow) + ":{" + tmpstr + "},"
            edges.append(tmpstr)
            tmpstr = ""

        # 返回节点、边、聚类数量
        for vv in nodes:
            if tmpstr == "":
                tmpstr = vv
            else:
                tmpstr = tmpstr + "\r\n" + vv
        # print(tmpstr)
        tmpstr2 = ""
        for vv in edges:
            if tmpstr2 == "":
                tmpstr2 = vv
            else:
                tmpstr2 = tmpstr2 + "\r\n" + vv
        # print(tmpstr2)
        # 返回节点、边、聚类数量
        ret = {"nodes": tmpstr, "edges": tmpstr2, "maxcluster": clusternum, "thetext":thetext1}
        return render(request, 'showclusterresult.html', {'ret': ret})
    else:
        # PUT,DELETE,HEAD,OPTION...
        return render(request, 'inputtext.html')


#共现层次聚类分析，生成
def genArborBymatrix(request):
    print("生成共现层次聚类分析结果",getIP(request))
    if request.method == "GET":
        #如果没有传数据，返回原界面
        return render(request, 'inputtext.html')
    elif request.method == "POST":
        thetext=request.POST.get('thetext')
        clusternum = request.POST.get('clusternum')
        wordliststr = request.POST.get('keywordlist')
        wordliststr=wordliststr.strip()
        wordliststr=wordliststr.replace("；"," ")
        wordliststr=wordliststr.replace(";"," ")
        wordliststr=wordliststr.replace("，"," ")
        wordliststr=wordliststr.replace("　"," ")
        wordliststr=wordliststr.replace("，"," ")
        wordliststr=wordliststr.replace(","," ")
        wordliststr=wordliststr.replace("、"," ")
        wordliststr=wordliststr.replace("。"," ")
        wordliststr=wordliststr.replace("."," ")
        wordliststr=wordliststr.replace("  "," ")
        wordliststr=wordliststr.replace("  "," ")
        wordliststr=wordliststr.replace("  "," ")
        wordlist=wordliststr.split(" ")
        # 准备语料
        doclist = thetext.split("\r\n")

        # 首先生成字典
        coroccuarr = {}
        for vv in wordlist:
            onwordarr = {}
            for mm in wordlist:
                onwordarr[mm] = 0
            coroccuarr[vv] = onwordarr
        #print(coroccuarr)

        for vv in doclist:
            # 每行分词，计算共现次数
            vv = vv.strip()
            if (vv == ""):
                continue
            words = pseg.cut(vv)
            for j in words:
                if j.word in wordlist:
                    for k in words:
                        if k.word in wordlist:
                            coroccuarr[j.word][k.word] = coroccuarr[j.word][k.word] + 1
        #计算节点权重
        thetext1 = request.POST.get('thetext')
        thetext = thetext.replace("\r\n", "")
        stopwords = request.POST.get('stopwords')
        if stopwords:
            stopwords = stopwords.strip()
            stopwordslst = stopwords.split("\r\n")
        else:
            stopwordslst = None
        dictwords = request.POST.get('dictwords')
        if dictwords:
            dictwords = dictwords.strip()
            dictwordslst = dictwords.split("\r\n")
        else:
            dictwordslst = None

        wordweight = {}
        maxwordweight = 0
        minwordweight = 0
        '''
        mytfidf = tfidf.TFIDF()
        keywords_tfidf = mytfidf.extract_tags(thetext, topK=None, withWeight=True, withFlag=False)  # allowPOS=('n','x','nr','ns','nsf','nt','nl','ng','s','v')  allowPOS默认为('ns', 'n', 'vn', 'v')
        '''
        keywords_tfidf = gettextkeywords(text=thetext, num=None, withWeight=True, allowPos=None,stopwordslst=stopwordslst,dictwordslst=dictwordslst)
        for vv in keywords_tfidf:
            if vv[0] in wordlist:
                wordweight[vv[0]] = vv[1]
                if vv[1] > maxwordweight:
                    maxwordweight = vv[1]
                if vv[1] < minwordweight:
                    minwordweight = vv[1]
        # 规一化为40-120
        minnormal = 40
        maxnormal = 150
        for vv in wordweight:
            wordweight[vv] = minnormal + (wordweight[vv] * (maxnormal - minnormal)) / (maxwordweight - minwordweight)

        ret = genClusterFromMatrix(coroccuarr, wordweight,int(clusternum),thetext1)
        return render(request, 'showclusterresult.html',{'ret':ret})
    else:
        # PUT,DELETE,HEAD,OPTION...
        return render(request, 'inputtext.html')
#显示关键词相关网络分析图
def genArborByCorelation(request):
    print("生成关键词相关网络分析结果",getIP(request))
    if request.method == "GET":
        # 如果没有传数据，返回原界面
        return render(request, 'inputtext.html')
    elif request.method == "POST":
        thetext = request.POST.get('thetext')
        clusternum = request.POST.get('clusternum')
        wordliststr = request.POST.get('keywordlist')
        wordliststr=wordliststr.strip()
        wordliststr=wordliststr.replace("；"," ")
        wordliststr=wordliststr.replace(";"," ")
        wordliststr=wordliststr.replace("，"," ")
        wordliststr=wordliststr.replace("　"," ")
        wordliststr=wordliststr.replace("，"," ")
        wordliststr=wordliststr.replace(","," ")
        wordliststr=wordliststr.replace("、"," ")
        wordliststr=wordliststr.replace("。"," ")
        wordliststr=wordliststr.replace("."," ")
        wordliststr=wordliststr.replace("  "," ")
        wordliststr=wordliststr.replace("  "," ")
        wordliststr=wordliststr.replace("  "," ")
        wordlist=wordliststr.split(" ")

        # 得到关系矩阵
        # 最宽线条为10
        lineweight=4
        coroccuarr = {}
        for vv in wordlist:
            onwordarr = {}
            for mm in wordlist:
                tmpval=getWordRelationNumber(vv,mm)*lineweight
                onwordarr[mm] = tmpval
            coroccuarr[vv] = onwordarr
        #规一化关系矩阵，不用，关系取得0-1

        #计算权重，表示为大小
        thetext1 = request.POST.get('thetext')
        thetext = thetext.replace("\r\n", "")
        
        stopwords = request.POST.get('stopwords')
        if stopwords:
            stopwords = stopwords.strip()
            stopwordslst = stopwords.split("\r\n")
        else:
            stopwordslst = None
        dictwords = request.POST.get('dictwords')
        if dictwords:
            dictwords = dictwords.strip()
            dictwordslst = dictwords.split("\r\n")
        else:
            dictwordslst = None
        wordweight = {}
        maxwordweight = 0
        minwordweight = 0
        '''
        mytfidf = tfidf.TFIDF()
        keywords_tfidf = mytfidf.extract_tags(thetext, topK=None, withWeight=True, withFlag=False)  # allowPOS=('n','x','nr','ns','nsf','nt','nl','ng','s','v')  allowPOS默认为('ns', 'n', 'vn', 'v')
        '''
        keywords_tfidf = gettextkeywords(text=thetext, num=None, withWeight=True, allowPos=None,stopwordslst=stopwordslst,dictwordslst=dictwordslst)
        for vv in keywords_tfidf:
            if vv[0] in wordlist:
                wordweight[vv[0]] = vv[1]
                if vv[1] > maxwordweight:
                    maxwordweight = vv[1]
                if vv[1] < minwordweight:
                    minwordweight = vv[1]
        # 规一化为40-120
        minnormal = 40
        maxnormal = 120
        for vv in wordweight:
            wordweight[vv] = minnormal + (wordweight[vv] * (maxnormal - minnormal)) / (maxwordweight - minwordweight)

        # 根据共现矩阵，产生网络结构图
        # 首先产生节点
        # 节点与连线
        nodes = []
        edges = []
        irow = 0
        #print('coroccuarr',coroccuarr)
        for mm in coroccuarr:
            tmpstr = ""
            icol = 0
            for nn in coroccuarr:
                #最小共现次数
                if coroccuarr[mm][nn]*(100/lineweight) > float(clusternum):
                    tmpstr = tmpstr + str(icol) + ":{weight:" + str(int(coroccuarr[mm][nn])) + ",},"
                icol = icol + 1
            if tmpstr != "":
                tmpstr = str(irow) + ":{" + tmpstr + "},"
                edges.append(tmpstr)
            if mm in wordweight:
                nodes.append(str(irow) + ":{'shape':'dot','label':'"+mm+"','group':'1','radius':"+str(wordweight[mm])+",'level':1,'show':1,},")
            else:
                nodes.append(str(irow) + ":{'shape':'dot','label':'"+mm+"','group':'1','radius':60,'level':1,'show':1,},")

            irow = irow + 1
        tmpstr = ""
        for vv in nodes:
            if tmpstr == "":
                tmpstr = vv
            else:
                tmpstr = tmpstr + "\r\n" + vv
        # print(tmpstr)
        tmpstr2 = ""
        for vv in edges:
            if tmpstr2 == "":
                tmpstr2 = vv
            else:
                tmpstr2 = tmpstr2 + "\r\n" + vv
        # print(tmpstr2)
        # 返回节点、边、聚类数量
        ret = {"nodes": tmpstr, "edges": tmpstr2, "maxcluster": clusternum, "thetext":thetext1}
        return render(request, 'showclusterresult.html', {'ret': ret})
    else:
        # PUT,DELETE,HEAD,OPTION...
        return render(request, 'inputtext.html')

#显示共现矩阵网络图
def genArborByCoocurrence(request):
    print("生成共现分析结果聚糖类分析结果",getIP(request))
    if request.method == "GET":
        # 如果没有传数据，返回原界面
        return render(request, 'inputtext.html')
    elif request.method == "POST":
        thetext = request.POST.get('thetext')
        clusternum = request.POST.get('clusternum')
        wordliststr = request.POST.get('keywordlist')
        wordliststr=wordliststr.strip()
        wordliststr=wordliststr.replace("；"," ")
        wordliststr=wordliststr.replace(";"," ")
        wordliststr=wordliststr.replace("，"," ")
        wordliststr=wordliststr.replace("　"," ")
        wordliststr=wordliststr.replace("，"," ")
        wordliststr=wordliststr.replace(","," ")
        wordliststr=wordliststr.replace("、"," ")
        wordliststr=wordliststr.replace("。"," ")
        wordliststr=wordliststr.replace("."," ")
        wordliststr=wordliststr.replace("  "," ")
        wordliststr=wordliststr.replace("  "," ")
        wordliststr=wordliststr.replace("  "," ")
        wordlist=wordliststr.split(" ")
        # 准备语料
        doclist = thetext.split("\r\n")

        # 首先生成字典
        coroccuarr = {}
        for vv in wordlist:
            onwordarr = {}
            for mm in wordlist:
                onwordarr[mm] = 0
            coroccuarr[vv] = onwordarr
        #最大共现次数
        maxocc=0
        for vv in doclist:
            # 每行分词，计算共现次数
            vv = vv.strip()
            if (vv == ""):
                continue
            words = pseg.cut(vv)
            for j in words:
                if j.word in wordlist:
                    for k in words:
                        if k.word in wordlist:
                            coroccuarr[j.word][k.word] = coroccuarr[j.word][k.word] + 1
                            if(coroccuarr[j.word][k.word]>maxocc):
                                #记录最大共现次数
                                maxocc=coroccuarr[j.word][k.word]
        #计算权重
        thetext1 = request.POST.get('thetext')
        thetext = thetext.replace("\r\n", "")
        stopwords = request.POST.get('stopwords')
        if stopwords:
            stopwords = stopwords.strip()
            stopwordslst = stopwords.split("\r\n")
        else:
            stopwordslst = None
        dictwords = request.POST.get('dictwords')
        if dictwords:
            dictwords = dictwords.strip()
            dictwordslst = dictwords.split("\r\n")
        else:
            dictwordslst = None
        wordweight = {}
        maxwordweight = 0
        minwordweight = 0
        '''
        mytfidf = tfidf.TFIDF()
        keywords_tfidf = mytfidf.extract_tags(thetext, topK=None, withWeight=True,withFlag=False)  # allowPOS=('n','x','nr','ns','nsf','nt','nl','ng','s','v')  allowPOS默认为('ns', 'n', 'vn', 'v')
        '''
        keywords_tfidf = gettextkeywords(text=thetext, num=None, withWeight=True, allowPos=None,stopwordslst=stopwordslst,dictwordslst=dictwordslst)
        for vv in keywords_tfidf:
            if vv[0] in wordlist:
                wordweight[vv[0]] = vv[1]
                if vv[1] > maxwordweight:
                    maxwordweight = vv[1]
                if vv[1] < minwordweight:
                    minwordweight = vv[1]
        # 规一化为40-120
        minnormal = 40
        maxnormal = 150
        for vv in wordweight:
            wordweight[vv] = minnormal + (wordweight[vv] * (maxnormal - minnormal)) / (maxwordweight - minwordweight)

        # 根据共现矩阵，产生网络结构图
        # 首先产生节点
        # 节点与连线
        nodes = []
        edges = []
        irow = 0
        #最宽线条
        maxweight=4
        for mm in coroccuarr:
            tmpstr = ""
            icol = 0
            for nn in coroccuarr:
                #最小共现次数
                if coroccuarr[mm][nn] > int(clusternum):
                    tmpstr = tmpstr + str(icol) + ":{weight:" + str(int(maxweight*coroccuarr[mm][nn]/maxocc)) + ",},"
                icol = icol + 1
            if tmpstr != "":
                tmpstr = str(irow) + ":{" + tmpstr + "},"
                edges.append(tmpstr)
            if mm in wordweight:
                nodes.append(str(irow) + ":{'shape':'dot','label':'"+mm+"','group':'1','radius':"+str(wordweight[mm])+",'level':1,'show':1,},")
            else:
                nodes.append(str(irow) + ":{'shape':'dot','label':'"+mm+"','group':'1','radius':60,'level':1,'show':1,},")

            irow = irow + 1
        tmpstr = ""
        for vv in nodes:
            if tmpstr == "":
                tmpstr = vv
            else:
                tmpstr = tmpstr + "\r\n" + vv
        # print(tmpstr)
        tmpstr2 = ""
        for vv in edges:
            if tmpstr2 == "":
                tmpstr2 = vv
            else:
                tmpstr2 = tmpstr2 + "\r\n" + vv
        # print(tmpstr2)
        # 返回节点、边、聚类数量
        ret = {"nodes": tmpstr, "edges": tmpstr2, "maxcluster": clusternum, "thetext":thetext1}
        return render(request, 'showclusterresult.html', {'ret': ret})
    else:
        # PUT,DELETE,HEAD,OPTION...
        return render(request, 'inputtext.html')

def register(request):

    return render(request, 'register.html')

def hz_gengraph(request):
    if request.method == "GET":
        #如果没有传数据，返回原界面
        return render(request, 'hanzi/inputtext.html')
    elif request.method == "POST":
        thelist = request.POST.getlist('keywordlist')
        clusternum = request.POST.get('clusternum')
        ret = gencluster(thelist, int(clusternum))
        return render(request, 'hanzi/showclusterresult.html',{'ret':ret})
    else:
        # PUT,DELETE,HEAD,OPTION...
        return render(request, 'hanzi/inputtext.html')
