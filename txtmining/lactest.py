import paddlehub as hub
#--------------------------------------------
#功能1:词法分析:用于将一个数组中的所有字符串分词并标注词性
#--------------------------------------------
#输入参数:要分析的字符串数组[str1,str2,str3...]
#输出:分析结果
def LACCut(texts):
    # 调用 PaddleHub 中现成的分词模型 LAC
    lac_moudle = hub.Module(name="lac")
    #返回的结果
    retstr=""
    results = lac_moudle.cut(text=texts, use_gpu=False, batch_size=1, return_tag=True)
    '''
    lac预测接口，预测输入句子的分词结果
    参数
        text(str or list): 待预测数据，单句预测数据（str类型）或者批量预测（list，每个元素为str
        use_gpu(bool): 是否使用GPU预测，如果使用GPU预测，则在预测之前，请设置CUDA_VISIBLE_DEVICES环境变量，否则不用设置
        batch_size(int): 批处理大小
        return_tag(bool): 预测结果是否需要返回分词标签结果
        # 返回结果：results(list): 分词结果是个列表
        print(texts)
    '''
    return results
#应用举例
# 要分析的素材

raw_data = [  ["刘莉，女，讲师，硕士生导师，哲学博士。主要从事课程研究、教师教育、教育研究方法等领域的教学与科研工作。主持教育部人文社科基金项目、捷克资助的教师教育国际比较研究项目，主持四川师范大学“质量工程”精品资源共享课建设校级教改项目，参与国家社科基金项目、欧盟Erasmus+项目等多项课题的研究工作；参与欧洲教育研究学会举办学术年会 （“ECER 2013- Creativity and Innovation in Educational Research”）、欧洲教育课程改革中心等机构主办的学术会议，作分论坛学术报告，并先后到德国、西班牙、土耳其等国家的高校进行访学交流；在《教育学报》等中文核心期刊及国外教育类学术期刊发表论文近十篇，其论文成果被人大复印资料《教育学》等全文转载。"]
]
dict_tagName={"n":"普通名词","f":"方位名词","s":"处所名词","t":"时间","nr":"人名","ns":"地名","nt":"机构名","nw":"作品名","nz":"其他专名","v":"普通动词","vd":"动副词","vn":"名动词","a":"形容词","ad":"副形词","an":"名形词","d":"副词","m":"数量词","q":"量词","r":"代词","p":"介词","c":"连词","u":"助词","xc":"其他虚词","w":"标点符号","PER":"人名","LOC":"地名","ORG":"机构名","TIME":"时间"}
for texts in raw_data: # 每一次取一个列表中的 元素，这个 元素 是个 字符串 的 列表
    results=LACCut(texts)
    retstr=""
    # 这里单个分词 的结果是个字典，其中两个key，一个是分词结果 "word"，一个是词性标注 "tag"
    for result in results: # 取得结果列表中的一个元素
        if(retstr==""):
            pass
        else:
            retstr=retstr+"\r"
        lst_word=result["word"]
        lst_tag=result["tag"]
        icnt=0
        for vv in lst_word:
            retstr=retstr+vv+"["+dict_tagName[lst_tag[icnt]]+"] \r\n"
            icnt=icnt+1
    print(retstr)

