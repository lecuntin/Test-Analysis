import jieba
import jieba.posseg as pseg
jieba.enable_parallel(4)
# 利用结巴做词性标准

def jiebaCiXingBiaoZhu():

    fp = open('/home/liux/develop/txtmining/txtmining/data/xxhzc_org.txt', 'r', encoding='utf-8')  # 打开文件
    linecnt=0
    #{"大":{"adj":10,"n":2},"癍“:{}}
    worddict={}
    for line in fp.readlines(): # 读取全文本
        '''
        if linecnt>100:
            break
        else:
            linecnt+=1
        '''
        line = line.strip('\n')
        # print(line)
        words = pseg.cut(line)
        for j in words:
            if j.word in worddict.keys():
                oneword=worddict[j.word]
                if j.flag in oneword.keys():
                    oneword[j.flag]=oneword[j.flag]+1
                else:
                    oneword[j.flag]=1
            else:
                worddict[j.word]={j.flag:1}
            #print('%s,%s' % (j.word, j.flag))


    # 保存字典
    f = open("data/worddict.txt", 'w')
    f.write(str(worddict))
    f.close()
    print("save dict successfully.")

    # 读取字典
    f = open("data/worddict.txt", 'r')
    dict_ = eval(f.read())
    f.close()
    print("read from local : ", dict_)

    fp.close()


if __name__ == '__main__':
    jiebaCiXingBiaoZhu()
