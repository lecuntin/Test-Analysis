#from fasttext import load_model
#fasttext_model = load_model(r'model/model_wiki_skipgram.bin')
#
#lst=fasttext_model.get_nearest_neighbors('清醒',50)
#for vv in lst:
#	print(vv[1])


# -*- coding: utf-8 -*-
import os

import fasttext
import jieba
import numpy as np
#import tqdm

base_path = os.path.dirname(os.path.abspath(__file__))
# 我这里使用的 sqlite 连接的某个数据库
# 下面用的表是聊天数据的表，其中我只会使用到 sentence 也就是聊天内容的字段
database_path = os.path.dirname(base_path)
database_dir = os.path.join(database_path, "datas", "data.db")

# 加载 jieba 分词词典
#jieba.load_userdict(os.path.join(base_path, "lcut.txt"))
#jieba.load_userdict(os.path.join(base_path, "500000-dict.txt"))

model = fasttext.load_model('data/model_wiki_skipgram.bin')

def get_word_vector(word):
    #”获取某词词向量“”“
    
    word_vector = model.get_word_vector(word)

    return word_vector

def get_sentence_vector(sentence):
    #	”“”获取某句句向量“”“
    cut_words = jieba.lcut(sentence)

    sentence_vector = None
    
    for word in cut_words:
        word_vector = get_word_vector(word)

        if sentence_vector is not None:
            sentence_vector += word_vector
        else:
            sentence_vector = word_vector

    sentence_vector = sentence_vector / len(cut_words)

    return sentence_vector


def cos_sim(vector_a, vector_b):
    """
    计算两个向量之间的余弦相似度
    :param vector_a: 向量 a
    :param vector_b: 向量 b
    :return: sim
    """
    vector_a = np.mat(vector_a)
    vector_b = np.mat(vector_b)
    num = float(vector_a * vector_b.T)
    denom = np.linalg.norm(vector_a) * np.linalg.norm(vector_b)
    cos = num / denom
    sim = 0.5 + 0.5 * cos
    return sim

if __name__ == "__main__":
    stra="东方"
    strb="春天"
    strc="冬天"
    a = get_sentence_vector(stra)
    b = get_sentence_vector(strb)
    c = get_sentence_vector(strc)
    print(stra,strb,strc)
    print('a->b',cos_sim(a, b))
    print('b->c',cos_sim(b, c))
    print('c->a',cos_sim(c, a))


