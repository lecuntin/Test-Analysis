#!/usr/bin/python
# -*- coding:utf-8 -*-
#author: lingyue.wkl
#desc: use to read ini
#---------------------
#2012-02-18 created
#2012-09-02 changed for class support
#---------------------
import sys,os,time
import configparser

class Config:
    def __init__(self, path=None ):
        self.path
        self.cf = configparser.ConfigParser()
        self.cf.read(self.path)
    def get(self, field, key):
        result = ""
        try:
            result = self.cf.get(field, key)
        except:
            result = ""
        return result
    def set(self, filed, key, value):
        try:
            self.cf.set(field, key, value)
            self.cf.write(open(self.path,'w'))
        except:
            return False
        return True
def read_config(config_file_path, field, key):
    #print(config_file_path, field, key)
    cf = configparser.ConfigParser()
    try:
        cf.read(config_file_path)
        result = cf.get(field, key)
    except:
        sys.exit(1)
    return result

def write_config(config_file_path, field, key, value):
    cf = configparser.ConfigParser()
    try:
        cf.read(config_file_path)
        cf.set(field, key, value)
        cf.write(open(config_file_path,'w'))
    except:
        sys.exit(1)
    return True
if __name__ == "__main__":
   config_file_path = "system.ini"
   field = "register"
   key1 = "allowRegister"
   key2="inviteCode"
   if(bool(read_config(config_file_path, field, "allowregister"))==True):
       if(bool(read_config(config_file_path, field, "needInviteCode"))==True):
           if(read_config(config_file_path, field, "inviteCode")=="3233412"):
               print("可以注册了")

