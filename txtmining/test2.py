# -*- coding: utf-8 -*-
# @Time     :   2018/6/21 23:18
# @Author   :   kingloon
# @Site     :
# @File     :   K-means.py

from numpy import *
import matplotlib.pyplot as plt

modelfilename = 'data/model_XXHZC_skipgram.bin'
from fasttext import load_model
model = load_model(modelfilename)

# 求向量距离
def distEclud(vecA, vecB):
    return sqrt(sum(power(vecA - vecB, 2)))


# 随机生成k个点作为初始质心
# 若选择随机生成的点作为初始质心，则有可能导致后面更新质心时出现有的质心为 NaN(非数) 的情况
# def initCent(dataSet, k):
#     n = shape(dataSet)[1]  # n是列数
#     centroids = mat(zeros((k, n)))
#     for j in range(n):
#         minJ = min(dataSet[:, j])  # 找到第j列最小值
#         rangeJ = max(dataSet[:, j]) - minJ  # 求第j列最大值与最小值的差
#         centroids[:, j] = minJ + random.rand(k, 1) * rangeJ  # 生成k行1列的在(0, 1)之间的随机数矩阵
#     return centroids

# 选前k个点作为初始质心
def initCent(dataSet, k):
    data = []
    for i in range(k):
        data.append(dataSet[i].tolist())
    a = array(data)
    centroids = mat(a)
    return centroids


# K均值聚类算法实现
def KMeans(dataSet, k, distMeas=distEclud):
    m = shape(dataSet)[0]  # 数据集的行
    clusterAssment = mat(zeros((m, 2)))
    centroids = initCent(dataSet, k)
    clusterChanged = True
    while clusterChanged:
        clusterChanged = False
        for i in range(m):  # 遍历数据集中的每一行数据
            minDist = inf
            minIndex = -1
            for j in range(k):  # 寻找最近质心
                distJI = distMeas(centroids[j, :], dataSet[i, :])
                if distJI < minDist:  # 更新最小距离和质心下标
                    minDist = distJI
                    minIndex = j
            if clusterAssment[i, 0] != minIndex:
                clusterChanged = True
            clusterAssment[i, :] = minIndex, minDist ** 2  # 记录最小距离质心下标，最小距离的平方
        for cent in range(k):  # 更新质心位置
            ptsInClust = dataSet[nonzero(clusterAssment[:, 0].A == cent)[0]]  # 获得距离同一个质心最近的所有点的下标，即同一簇的坐标
            centroids[cent, :] = mean(ptsInClust, axis=0)  # 求同一簇的坐标平均值，axis=0表示按列求均值

    return centroids, clusterAssment


# 取数据的前两维特征作为该条数据的x , y 坐标，
def getXY(dataSet):
    import numpy as np
    m = shape(dataSet)[0]  # 数据集的行
    X = []
    Y = []
    for i in range(m):
        X.append(dataSet[i, 0])
        Y.append(dataSet[i, 1])
    return np.array(X), np.array(Y)


# 数据可视化
def showCluster(dataSet, k, clusterAssment, centroids):
    fig = plt.figure()
    plt.title("K-means")
    ax = fig.add_subplot(111)
    data = []

    for cent in range(k):  # 提取出每个簇的数据
        ptsInClust = dataSet[nonzero(clusterAssment[:, 0].A == cent)[0]]  # 获得属于cent簇的数据
        data.append(ptsInClust)

    for cent, c, marker in zip(range(k), ['r', 'g', 'b', 'y'], ['^', 'o', '*', 's']):  # 画出数据点散点图
        X, Y = getXY(data[cent])
        ax.scatter(X, Y, s=80, c=c, marker=marker)

    centroidsX, centroidsY = getXY(centroids)
    ax.scatter(centroidsX, centroidsY, s=1000, c='black', marker='+', alpha=1)  # 画出质心点
    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    plt.show()


if __name__ == "__main__":
    cluster_Num = 3
    wordlist = ["学校", "人", "老师", "教育", "学生", "资源", "钱", "城市", "高中", "班", "地方", "农村", "初中", "教师", "幼儿园", "乡镇", "东西",
                "公司", "发展", "工作", "技术", "编制", "政策", "事情", "规模", "人才", "小学", "校长", "国家", "招生", "政府", "建设",
                "情况", "管理", "经济", "时间", "核心", "资金", "中学", "平台", "集团", "企业", "思想", "大学", "孩子", "老百姓", "质量", "城区",
                "家长", "房子", "教学", "数据", "环境", "知识", "联盟", "关系", "层次", "水平", "状态", "省", "社会", "优势", "地区", "市场",
                "执行", "标准", "样子", "活动", "基础", "学位", "家", "当地", "成绩", "现实", "生源", "系统", "训练", "产品", "人口",
                "人员", "优化", "信息", "区域", "县", "布局", "年级", "影响", "待遇", "指标", "支撑", "教育局", "理解", "研究", "统计",
                "装备", "镇", "交流", "体系", "原因", "外头", "学科", "新区", "方式", "条件", "版本", "目的", "结构", "计划", "设计", "调查",
                "部门", "集团化", "项目", "中央", "中层", "乡", "办法", "园区", "娃娃", "小区", "年龄", "户籍", "文件", "方法", "服务", "机器",
                "理想", "过程", "不愿意", "专业", "专家", "中院", "价值", "住宿", "关键", "农民", "出口", "分校", "压力",
                "同学", "城镇", "城镇化", "外面", "娃儿", "学院", "层面", "工资", "数学", "材料", "比例", "现象", "理论", "生活",
                "目标", "矛盾", "竞争", "经费", "网上", "考试", "观念", "话", "重点", "销售", "阶段", "云", "别人", "力量", "后勤", "复读生", "子女",
                "家庭", "岗位", "差异", "市委", "市政府", "思路", "想法", "战略", "投资", "改革", "机制", "档案", "检查",
                "检测", "概念", "理念", "空间", "网站", "网络", "考核", "覆盖", "规范", "角度", "设备", "资料", "面试",
                "食堂", "高考", "一贯制", "儿童", "党员", "入口", "分配", "制度", "合同", "启发", "周边", "学期",
                "实力", "山区", "工作量", "工程师", "师傅", "师资", "当中", "总会", "想象", "成本", "手续", "技能", "政治", "教室", "教授", "教材",
                "普高", "机会", "机构", "校园", "模式", "民族", "焦虑", "父母", "现场", "班上", "班级", "理由", "督导", "研究生", "程度", "级",
                "群众", "群体", "老家", "老师们", "考级", "观点", "规律", "话题", "课程", "财经"]
    maxcluster = 3
    wordvect = []

    for vv in wordlist:
        wordvect.append(model[vv])

    centroids, clusterAssment = KMeans(wordvect, maxcluster)
    showCluster(wordvect, maxcluster, clusterAssment, centroids)