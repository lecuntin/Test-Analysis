// 是否需要(允许)处理鼠标的移动事件,默认识不处理
var select = false;

// 设置默认值,目的是隐藏图层
$("#rect").css("width", "0px;");
$("#rect").css("height", "0px;");
//让你要画的图层位于最上层
$("#rect").css("z-index", "10000000");

// 记录鼠标按下时的坐标
var startX = 0;
var startY = 0;
// 记录鼠标抬起时候的坐标
var endX = startX;
var endY = startY;

var rectItems;

function getXY(objLookingFor) 
{
    var posX = objLookingFor.offsetLeft;
    var posY = objLookingFor.offsetTop;
    var aBox = objLookingFor;//需要获得位置的对象
    do {
        aBox = aBox.offsetParent;
        posX += aBox.offsetLeft;
        posY += aBox.offsetTop;
    } 
    while( aBox.tagName != "BODY" );
    return {left:posX,top:posY}
}

//处理鼠标按下事件
function down(event) {
    var obj = window.event ? event.srcElement : evt.target;
    if (obj.id == "BottomRightCenterContentDiv") {
        //重置容器框初始样式
        $("#rect").attr("style", "position: absolute; background-color: rgb(195, 213, 237);opacity:0.6;border: 1px dashed rgb(0, 153, 255);");
        $("#rect").empty();

        // 鼠标按下时才允许处理鼠标的移动事件
        select = true;
        // 取得鼠标按下时的坐标位置

        var e = event || window.event;
        var scrollX = document.documentElement.scrollLeft || document.body.scrollLeft;
        var scrollY = document.documentElement.scrollTop || document.body.scrollTop;
        var x = e.pageX || e.clientX + scrollX;
        var y = e.pageY || e.clientY + scrollY;
        startX = x;
        startY = y;

        //设置你要画的矩形框的起点位置
        $("#rect").offset().left = startX;
        $("#rect").offset().top = startY;
    }
}

//鼠标抬起事件
function up(event) {
    if (select) {
        var l=getXY(document.all.rect).left
        var t=getXY(document.all.rect).top
        var w=parseInt(document.getElementById('rect').style.width);
        var h=parseInt(document.getElementById('rect').style.height);

        if (w > 10 && h > 10) {
            $("#rect").css("display", "block");
            rectItems = new Array();

            //查找被框选中的元素  ------start
            var obj = $("#BottomRightCenterContentDiv");

            $("span[name='cont']").each(function(){ 

        var l1=getXY(this).left
        var t1=getXY(this).top
        var w1=parseInt(this.style.width);
        var h1=parseInt(this.style.height);
/*
                var l1= this.offsetLeft
                var t1= this.offsetTop
                var w1 = this.offsetWidth;
                var h1 = this.offsetHeight;
*/
                if(l1>l && t1>t && 
                ((l1+w1) < (l+w)) && 
                ((t1+h1)< t+h))
                    this.children[0].checked=!this.children[0].checked
            });
            $("#rect").css("display", "none");

            //重新设置框选容器的大小、位置，并将被框选中的元素移入框选容器   -------end
        } 
        else 
        {
            $("#rect").css("display", "none");
        }
    }
    //鼠标抬起,就不允许在处理鼠标移动事件
    select = false;

    //AddBorder($("#rect"));
}

//鼠标移动事件,最主要的事件
function move(event) {
    /*
    这个部分,根据你鼠标按下的位置,和你拉框时鼠标松开的位置关系,可以把区域分为四个部分,根据四个部分的不同,
    我们可以分别来画框,否则的话,就只能向一个方向画框,也就是点的右下方画框.
    */
    if (select) {
        // 取得鼠标移动时的坐标位置

        var e = event || window.event;
        var scrollX = document.documentElement.scrollLeft || document.body.scrollLeft;
        var scrollY = document.documentElement.scrollTop || document.body.scrollTop;
        console.log(scrollX,scrollY)
        var x = e.pageX || e.clientX + scrollX;
        var y = e.pageY || e.clientY + scrollY;

        endX = x
        endY = y

        var r = $("#rect");
        // 设置拉框的大小
        $("#rect").width(Math.abs(endX - startX));
        $("#rect").height(Math.abs(endY - startY));

        $("#rect").css("display", "block");

        // A part
        if (endX < startX && endY < startY) {
            $("#rect").css("left", endX);
            $("#rect").css("top", endY);
        }

        // B part
        if (endX > startX && endY < startY) {
            $("#rect").css("left", startX);
            $("#rect").css("top", endY);
        }

        // C part
        if (endX < startX && endY > startY) {
            $("#rect").css("left",endX);
            $("#rect").css("top", startY);
        }

        // D part
        if (endX > startX && endY > startY) {
            $("#rect").css("left", startX);
            $("#rect").css("top", startY);
        }

        /*
        这两句代码是最重要的,没有这两句代码,你的拉框,就只能拉框,在鼠标松开的时候,
        拉框停止,但是不能相应鼠标的mouseup事件.那么你想做的处理就不能进行.
        这两句的作用是使当前的鼠标事件不在冒泡,也就是说,不向其父窗口传递,所以才可以相应鼠标抬起事件,
        这两行代码是拉框最核心的部分.
        */
        window.event.cancelBubble = true;
        window.event.returnValue = false;
    }

}

function AddBorder(obj) {
    $(obj).append($('<div id="ui-draggable-e1" class="ui-draggable-border" style="height: 100%; width: 5px;  right:0px;bottom: 0px; "></div>'));
    $(obj).append($('<div id="ui-draggable-n1" class="ui-draggable-border" style="height: 5px; width: 100%;  top:0px;"></div>'));
    $(obj).append($('<div id="ui-draggable-w1" class="ui-draggable-border" style="height: 100%; width: 5px; bottom: 0px;"></div>'));
    $(obj).append($('<div id="ui-draggable-s1" class="ui-draggable-border" style="height: 5px; width: 100%; bottom:0px;"></div>'));
    $(obj).append($('<div id="ui-resizable-e" class="ui-resizable-handle ui-resizable-e" style="z-index: 1000;"></div>'));
    $(obj).append($('<div id="ui-resizable-s" class="ui-resizable-handle ui-resizable-s" style="z-index: 1000;"></div>'));
    $(obj).append($('<div id="ui-resizable-se" class="ui-resizable-handle ui-resizable-se" style="z-index: 1000;"></div>'));
    $(obj).append($('<div id="ui-resizable-n" class="ui-resizable-handle ui-resizable-n" style="z-index: 1000;"></div>'));
    $(obj).append($('<div id="ui-resizable-w" class="ui-resizable-handle ui-resizable-w" style="z-index: 1000;"></div>'));
    $(obj).append($('<div id="ui-resizable-ne" class="ui-resizable-handle ui-resizable-ne" style="z-index: 1000;"></div>'));
    $(obj).append($('<div id="ui-resizable-nw" class="ui-resizable-handle ui-resizable-nw" style="z-index: 1000;"></div>'));
    $(obj).append($('<div id="ui-resizable-sw" class="ui-resizable-handle ui-resizable-sw" style="z-index: 1000;"></div>'));
}

