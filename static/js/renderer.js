(function(){
		
		//JS实现字典
		function Dictionary()
		{
	        this.add = add;
		    this.datastore = new Object();
		    this.find = find;
		    this.remove = remove;
		    this.showAll = showAll;
		    this.length = length;
		    this.count = count;
		    this.clear = clear;
		    this.sort = sort;
		}
		function add(key, value){
		    this.datastore[key] = value;
		}
		function find(key){
		    return this.datastore[key];
		}
		function remove(key){
		    delete this.datastore[key];
		}
		function count()
		{
		    var n = 0;
		    for (var key in this.datastore)
		    {
		        ++n;
		    }
		    return n;
		}
		function showAll(){
		    for (var key in this.datastore) {
		        document.write(key + '->' + this.datastore[key]);
		        document.write('<br>');
		    }
		}
		function sort()
		{
		    var keys = Array();
		    for (var key in this.datastore)
		    {
		        keys.push( key );
		    }
		    keys.sort();
		    for (var i=0; i<keys.length; i++)
		    {
		        document.write(keys[i] + '->' + this.datastore[keys[i]]);
		        document.write('<br>');
		    }
		}
		function clear()
		{
		    for (var key in this.datastore)
		    {
		        delete this.datastore[key];
		    }
		}
		
		var dict_nodecolor = new Dictionary();
		//指定色系
		colorlist=["#A52A2A","#2F4F4F","#696969","#191970","#0000CD","#4682B4","#483D8B","#006400","#BC8F8F","#8669B4","#473C8B","#00688B","#458B74","#8B6914","#8B4726","#CD2626","#CD1076","#7D26CD","#00008B","#008B8B","#8B008B","#8B0000","#00CED1","#2F4F4F","#008B8B","#48D1CC","#3CB371","#2E8B57","#98FB98","#8FBC8F","#32CD32","#00FF00","#228B22","#008000","#006400","#7FFF00","#7CFC00"]
        //colorlist=["#0871CE","#FF69B4","#DB7003","#FF1493","#C71585","#9400D3","#4B0082","#7B68EE","#6A5ACD","#483D8B","#F8F8FF","#0000FF","#0000CD","#191970","#00008B","#000080","#4169E1","#6495ED","#B0C4DE","#708090","#1E90FF","#F0F8FF","#4682B4","#87CEFA","#87CEEB","#00BFFF","#ADD8E6","#B0E0E6","#5F9EA0","#F0FFFF","#E1FFFF","#AFEEEE","#00FFFF","#00FFFF","#00CED1","#2F4F4F","#008B8B","#008080","#48D1CC","#40E0D0","#7FFFAA","#00FA9A","#F5FFFA","#00FF7F","#3CB371","#2E8B57","#F0FFF0","#90EE90","#98FB98","#8FBC8F","#32CD32","#00FF00","#228B22","#008000","#006400","#7FFF00","#7CFC00","#ADFF2F","#556B2F","#FFFFF0","#FFFF00","#808000","#BDB76B","#FFFACD","#EEE8AA","#F0E68C","#FFD700","#FFF8DC","#DAA520","#FFFAF0","#FDF5E6","#F5DEB3","#FFE4B5","#FFA500","#FFEFD5","#FFEBCD","#FFDEAD","#FAEBD7","#D2B48C","#DEB887","#FFE4C4","#FF8C00","#FAF0E6","#CD853F","#FFDAB9","#F4A460","#D2691E","#8B4513","#FFF5EE","#A0522D","#FF7F50","#FF4500","#E9967A","#FF6347","#FFE4E1","#FA8072","#FFFAFA","#F08080","#BC8F8F","#CD5C5C","#FF0000","#A52A2A","#B22222","#8B0000","#800000","#FFFFFF","#F5F5F5","#DCDCDC","#C0C0C0","#A9A9A9","#808080","#696969","#000000"]

		var i=0
		for(vv in colorlist)
		{
		    dict_nodecolor.add(i,colorlist[i])
		    i++
		}
  Renderer = function(canvas){
    var canvas = $(canvas).get(0)
    var ctx = canvas.getContext("2d");
    var gfx = arbor.Graphics(canvas)
    var particleSystem = null
    var curnode = null
    var nodewithedge = []
    controlHidenode=false
    var that = {
      init:function(system){
        particleSystem = system
        particleSystem.screenSize(canvas.width, canvas.height) 
        particleSystem.screenPadding(40)

        that.initMouseHandling()
      },
      getcurNode:function(system)
      {
      	return curnode
      },
      redraw:function(){
        if (!particleSystem) return

        gfx.clear() // convenience ƒ: clears the whole canvas rect

        // draw the nodes & save their bounds for edge drawing
        var nodeBoxes = {}
        particleSystem.eachNode(function(node, pt){
           var currentlevel = parseInt(document.all.currentlevel.value)
           var thealpha=(11-node.data.level)/10

          // node: {mass:#, p:{x,y}, name:"", data:{}}
          // pt:   {x:#, y:#}  node position in screen coords

          // determine the box size and round off the coords if we'll be 
          // drawing a text label (awful alignment jitter otherwise...)


          //层次太高的节点不显示
          if (node.data.level>currentlevel) 
          	return
          if(controlHidenode)
          {
              if (node.data.show)
              {
                if(node.data.show!=2)
                    return
              }
              else
                return
          }
          var label = node.data.label||""
          var w = ctx.measureText(""+label).width + 10
          if (!(""+label).match(/^[ \t]*$/)){
            pt.x = Math.floor(pt.x)
            pt.y = Math.floor(pt.y)
          }else{
            label = null
          }

          // draw a rectangle centered at pt
          //指定颜色，按组来指定

          if (node.data.color)
            ctx.fillStyle =	node.data.color
          else
            //ctx.fillStyle = "rgba(0,0,0,.2)"
            ctx.fillStyle = dict_nodecolor.find(node.data.group) //node.data.color
          //if (node.data.color=='none')
          //  ctx.fillStyle = "white"
//          ctx.fillStyle = dict_nodecolor.find(node.data.group) //node.data.color
          
          if (node.data.shape=='dot'){
          	//如果给定了大小，是用给定大小，否则用文本来确定大小
          	if(node.data.radius)
          	{
            	tmpsize=node.data.radius
            	gfx.oval(pt.x-tmpsize/2, pt.y-tmpsize/2, tmpsize,tmpsize, {fill:ctx.fillStyle,alpha:thealpha})
	            nodeBoxes[node.name] = [pt.x-tmpsize/2, pt.y-tmpsize/2, tmpsize,tmpsize]
          	}
          	else
          	{
            	gfx.oval(pt.x-w/2, pt.y-w/2, w,w, {fill:ctx.fillStyle,alpha:thealpha})
            	nodeBoxes[node.name] = [pt.x-w/2, pt.y-w/2, w,w]
          	}


          }else{
            gfx.rect(pt.x-w/2, pt.y-10, w,20, 4, {fill:ctx.fillStyle})
            nodeBoxes[node.name] = [pt.x-w/2, pt.y-11, w, 22]
          }

          // draw the text
          if (label){
            //自适应：根据文本数量决定字体大小
            if(label.length>3)
                ctx.font = "12px bolder 微软雅黑"
            else
                ctx.font = "14px bolder 微软雅黑"
            ctx.textAlign = "center"
            ctx.fillStyle = "#FFFFFF"
            //原为指定颜色
            if (node.data.color=='none') ctx.fillStyle = '#FFFFFF'
            ctx.fillText(label||"", pt.x, pt.y+4)
            //ctx.fillText(label||"", pt.x+1, pt.y+1)
          }
        })    			


        // draw the edges
        particleSystem.eachEdge(function(edge, pt1, pt2){
        	var currentlevel = parseInt(document.all.currentlevel.value)

          // edge: {source:Node, target:Node, length:#, data:{}}
          // pt1:  {x:#, y:#}  source position in screen coords
          // pt2:  {x:#, y:#}  target position in screen coords
        //层次
		if (edge.source.data.level > currentlevel || edge.target.data.level > currentlevel) 
		    return
					
		  //根据层次决定线条宽度
		  //指定了线宽，则不处理
		  if (edge.weight==null)
		  {
		    //没指定线宽，设置了线宽也成
		    if(edge.data.weight!=null)
		        edge.weight=edge.data.weight
		    else
		        //否则，计算线宽
		        edge.weight=((15-edge.source.data.level)/4)
		  }
          var weight = edge.weight
          var color = dict_nodecolor.find(edge.source.data.group)    //edge.data.color
          edge.length=edge.source.data.level*2+20
          if(controlHidenode)
          {
              if (edge.source.data.show!=0)
                edge.source.data.show=2
              if (edge.target.data.show!=0)
                edge.target.data.show=2
              edge.data.directed=true
          }
          if (!color || (""+color).match(/^[ \t]*$/)) color = null

          // find the start point
          var tail = intersect_line_box(pt1, pt2, nodeBoxes[edge.source.name])
          var head = intersect_line_box(tail, pt2, nodeBoxes[edge.target.name])
          ctx.save() 
            ctx.beginPath()
            //根据节点的层次决定其粗细
            ctx.lineWidth = (!isNaN(weight)) ? parseFloat(weight) : 1
            ctx.strokeStyle = (color) ? color : "#cccccc"
            ctx.fillStyle = null
            ctx.moveTo(tail.x, tail.y)
            //ctx.quadraticCurveTo((tail.x+head.x)*1.01/2,(tail.y+head.y)*1.02/2,head.x, head.y);
            ctx.lineTo(head.x, head.y)
            ctx.stroke()
          ctx.restore()

          // draw an arrowhead if this is a -> style edge
          if (edge.data.directed){
            ctx.save()
              // move to the head position of the edge we just drew
              var wt = !isNaN(weight) ? parseFloat(weight) : 5
              var arrowLength = 6 + wt
              var arrowWidth = 2 + wt
              ctx.fillStyle = (color) ? color : "#cccccc"
              ctx.translate(head.x, head.y);
              ctx.rotate(Math.atan2(head.y - tail.y, head.x - tail.x));

              // delete some of the edge that's already there (so the point isn't hidden)
              ctx.clearRect(-arrowLength/2,-wt/2, arrowLength/2,wt)

              // draw the chevron
              ctx.beginPath();
              ctx.moveTo(-arrowLength, arrowWidth);
              ctx.lineTo(0, 0);
              ctx.lineTo(-arrowLength, -arrowWidth);
              ctx.lineTo(-arrowLength * 0.8, -0);
              ctx.closePath();
              ctx.fill();
            ctx.restore()
          }
        })

      },
      initMouseHandling:function(){
        // no-nonsense drag and drop (thanks springy.js)
        selected = null;
        nearest = null;
        var dragged = null;
        var oldmass = 1
        // set up a handler object that will initially listen for mousedowns then
        // for moves and mouseups while dragging
        var handler = {

          clicked:function(e){
            var pos = $(canvas).offset();
            _mouseP = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)
            selected = nearest = dragged = particleSystem.nearest(_mouseP);
            curnode=selected
            if (dragged.node !== null) dragged.node.fixed = true
            $(canvas).bind('mousemove', handler.dragged)
            $(window).bind('mouseup', handler.dropped)
            return false
          },
          dragged:function(e){
            var old_nearest = nearest && nearest.node._id
            var pos = $(canvas).offset();
            var s = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)
            if (!nearest) return
            if (dragged !== null && dragged.node !== null){
              var p = particleSystem.fromScreen(s)
              dragged.node.p = p
            }

            return false
          },

          dropped:function(e){

            if (dragged===null || dragged.node===undefined) return
            if (dragged.node !== null) dragged.node.fixed = false
            dragged.node.tempMass = 50
            dragged = null
            selected = null
            $(canvas).unbind('mousemove', handler.dragged)
            $(window).unbind('mouseup', handler.dropped)
            _mouseP = null
            return false
          }
        }
        $(canvas).mousedown(handler.clicked);

      }

    }

    // helpers for figuring out where to draw arrows (thanks springy.js)
    var intersect_line_line = function(p1, p2, p3, p4)
    {
      var denom = ((p4.y - p3.y)*(p2.x - p1.x) - (p4.x - p3.x)*(p2.y - p1.y));
      if (denom === 0) return false // lines are parallel
      var ua = ((p4.x - p3.x)*(p1.y - p3.y) - (p4.y - p3.y)*(p1.x - p3.x)) / denom;
      var ub = ((p2.x - p1.x)*(p1.y - p3.y) - (p2.y - p1.y)*(p1.x - p3.x)) / denom;

      if (ua < 0 || ua > 1 || ub < 0 || ub > 1)  return false
      return arbor.Point(p1.x + ua * (p2.x - p1.x), p1.y + ua * (p2.y - p1.y));
    }

    var intersect_line_box = function(p1, p2, boxTuple)
    {
      var p3 = {x:boxTuple[0], y:boxTuple[1]},
          w = boxTuple[2],
          h = boxTuple[3]

      var tl = {x: p3.x, y: p3.y};
      var tr = {x: p3.x + w, y: p3.y};
      var bl = {x: p3.x, y: p3.y + h};
      var br = {x: p3.x + w, y: p3.y + h};

      return intersect_line_line(p1, p2, tl, tr) ||
            intersect_line_line(p1, p2, tr, br) ||
            intersect_line_line(p1, p2, br, bl) ||
            intersect_line_line(p1, p2, bl, tl) ||
            false
    }

    return that
  }    
  
})()